PGDMP         &            	    y            inventorycontrol    13.4    13.4 )    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    inventorycontrol    DATABASE     p   CREATE DATABASE inventorycontrol WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Argentina.1252';
     DROP DATABASE inventorycontrol;
                postgres    false            �            1259    16441 	   categoria    TABLE        CREATE TABLE public.categoria (
    id integer NOT NULL,
    nombre character varying NOT NULL,
    estado boolean NOT NULL
);
    DROP TABLE public.categoria;
       public         heap    postgres    false            �            1259    16439    categoria_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.categoria_id_seq;
       public          postgres    false    209            �           0    0    categoria_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.categoria_id_seq OWNED BY public.categoria.id;
          public          postgres    false    208            �            1259    16430    marca    TABLE     {   CREATE TABLE public.marca (
    id integer NOT NULL,
    nombre character varying NOT NULL,
    estado boolean NOT NULL
);
    DROP TABLE public.marca;
       public         heap    postgres    false            �            1259    16428    marca_id_seq    SEQUENCE     �   CREATE SEQUENCE public.marca_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.marca_id_seq;
       public          postgres    false    207            �           0    0    marca_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.marca_id_seq OWNED BY public.marca.id;
          public          postgres    false    206            �            1259    16408    producto    TABLE     h  CREATE TABLE public.producto (
    id integer NOT NULL,
    codigo integer NOT NULL,
    nombre character varying,
    descripcion character varying,
    id_marca integer NOT NULL,
    id_categoria integer NOT NULL,
    stock integer NOT NULL,
    precio real NOT NULL,
    estado_producto boolean NOT NULL,
    fecha_caduc date,
    foto character varying
);
    DROP TABLE public.producto;
       public         heap    postgres    false            �            1259    16406    producto_id_seq    SEQUENCE     �   CREATE SEQUENCE public.producto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.producto_id_seq;
       public          postgres    false    203            �           0    0    producto_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.producto_id_seq OWNED BY public.producto.id;
          public          postgres    false    202            �            1259    16419    sector    TABLE     |   CREATE TABLE public.sector (
    id integer NOT NULL,
    nombre character varying NOT NULL,
    estado boolean NOT NULL
);
    DROP TABLE public.sector;
       public         heap    postgres    false            �            1259    16417    sector_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sector_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.sector_id_seq;
       public          postgres    false    205            �           0    0    sector_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.sector_id_seq OWNED BY public.sector.id;
          public          postgres    false    204            �            1259    16397    usuarios    TABLE     8  CREATE TABLE public.usuarios (
    id integer NOT NULL,
    nombre character varying,
    contrasenia character varying NOT NULL,
    contacto character varying,
    correo character varying,
    estado_cuenta boolean,
    tipo_usuario character varying,
    usuario character varying,
    sector_emp integer
);
    DROP TABLE public.usuarios;
       public         heap    postgres    false            �            1259    16395    usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.usuario_id_seq;
       public          postgres    false    201            �           0    0    usuario_id_seq    SEQUENCE OWNED BY     B   ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuarios.id;
          public          postgres    false    200            C           2604    16444    categoria id    DEFAULT     l   ALTER TABLE ONLY public.categoria ALTER COLUMN id SET DEFAULT nextval('public.categoria_id_seq'::regclass);
 ;   ALTER TABLE public.categoria ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    209    209            B           2604    16433    marca id    DEFAULT     d   ALTER TABLE ONLY public.marca ALTER COLUMN id SET DEFAULT nextval('public.marca_id_seq'::regclass);
 7   ALTER TABLE public.marca ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            @           2604    16411    producto id    DEFAULT     j   ALTER TABLE ONLY public.producto ALTER COLUMN id SET DEFAULT nextval('public.producto_id_seq'::regclass);
 :   ALTER TABLE public.producto ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            A           2604    16422 	   sector id    DEFAULT     f   ALTER TABLE ONLY public.sector ALTER COLUMN id SET DEFAULT nextval('public.sector_id_seq'::regclass);
 8   ALTER TABLE public.sector ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            ?           2604    16400    usuarios id    DEFAULT     i   ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);
 :   ALTER TABLE public.usuarios ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200    201            �          0    16441 	   categoria 
   TABLE DATA           7   COPY public.categoria (id, nombre, estado) FROM stdin;
    public          postgres    false    209   <,       �          0    16430    marca 
   TABLE DATA           3   COPY public.marca (id, nombre, estado) FROM stdin;
    public          postgres    false    207   Y,       �          0    16408    producto 
   TABLE DATA           �   COPY public.producto (id, codigo, nombre, descripcion, id_marca, id_categoria, stock, precio, estado_producto, fecha_caduc, foto) FROM stdin;
    public          postgres    false    203   v,       �          0    16419    sector 
   TABLE DATA           4   COPY public.sector (id, nombre, estado) FROM stdin;
    public          postgres    false    205   �,       �          0    16397    usuarios 
   TABLE DATA              COPY public.usuarios (id, nombre, contrasenia, contacto, correo, estado_cuenta, tipo_usuario, usuario, sector_emp) FROM stdin;
    public          postgres    false    201   �,       �           0    0    categoria_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.categoria_id_seq', 1, false);
          public          postgres    false    208            �           0    0    marca_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.marca_id_seq', 1, false);
          public          postgres    false    206            �           0    0    producto_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.producto_id_seq', 1, false);
          public          postgres    false    202            �           0    0    sector_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.sector_id_seq', 1, false);
          public          postgres    false    204            �           0    0    usuario_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.usuario_id_seq', 3, true);
          public          postgres    false    200            M           2606    16449    categoria categoria_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.categoria DROP CONSTRAINT categoria_pkey;
       public            postgres    false    209            K           2606    16438    marca marca_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.marca
    ADD CONSTRAINT marca_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.marca DROP CONSTRAINT marca_pkey;
       public            postgres    false    207            G           2606    16416    producto producto_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.producto DROP CONSTRAINT producto_pkey;
       public            postgres    false    203            I           2606    16427    sector sector_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.sector
    ADD CONSTRAINT sector_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.sector DROP CONSTRAINT sector_pkey;
       public            postgres    false    205            E           2606    16405    usuarios usuario_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuario_pkey;
       public            postgres    false    201            O           2606    16461    producto fk_categoria    FK CONSTRAINT     �   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT fk_categoria FOREIGN KEY (id_categoria) REFERENCES public.categoria(id) NOT VALID;
 ?   ALTER TABLE ONLY public.producto DROP CONSTRAINT fk_categoria;
       public          postgres    false    209    2893    203            N           2606    16456    producto fk_marca    FK CONSTRAINT     {   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT fk_marca FOREIGN KEY (id_marca) REFERENCES public.marca(id) NOT VALID;
 ;   ALTER TABLE ONLY public.producto DROP CONSTRAINT fk_marca;
       public          postgres    false    207    2891    203            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x�3�L,�L��4426��CC\1z\\\ �(�     