package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaNuevoProducto;
import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.Marca;
import modelo.MarcaDAO;
import modelo.TextPrompt;

import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

public class VistaNuevoProducto extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtCodigo;
	private JTextField txtStock;
	private JTextField txtPrecio;
	private JTextField txtFechaCad;
	private JButton btnNuevaMarca;
	private JButton btnModificarMarca;
	private JButton btnBorrarMarca;
	private JButton btnNuevaCategoria;
	private JButton btnModificarCategoria;
	private JButton btnBorrarCategoria;
	private ControladorVistaNuevoProducto controlador;
	private JButton btnCancelar;
	private JButton btnRegistrar;
	private JButton btnImagen;
	private JRadioButton rdbtnSi;
	private JRadioButton rdbtnNo;
	private ButtonGroup botonesCad;
	private JTextArea textAreaDescripcion;
	private JComboBox cBoxMarca;
	private JComboBox cBoxCategoria;
	private JLabel lblverFoto;
	private JLabel lblAstCat;
	private JLabel lblAstMarc;
	private JLabel lblAstNomb;
	private JLabel lblAstCod;
	private JLabel lblAstStock;
	private JLabel lblAstPrec;
	private JLabel lblAstDescr;
	private JLabel lblAstCampoObl;
	private JLabel lblAstSeleccion;

	//prueba para guardar archivo imagen

	
	public VistaNuevoProducto(ControladorVistaNuevoProducto controlador) {
		botonesCad = new ButtonGroup();
		this.setControlador(controlador);
		setTitle("InventoryControl - NuevoProducto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 614, 523);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(474, 437, 89, 23);
		btnCancelar.addActionListener(controlador);
		contentPane.add(btnCancelar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(getControlador());
		btnRegistrar.setBounds(357, 437, 89, 23);
		contentPane.add(btnRegistrar);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(28, 47, 134, 20);
		txtNombre.addFocusListener(getControlador());
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(28, 21, 86, 14);
		contentPane.add(lblNombre);
		
		JLabel lblFoto = new JLabel("Foto:");
		lblFoto.setBounds(28, 371, 46, 14);
		contentPane.add(lblFoto);
		
		JLabel lblCodigo = new JLabel("C�digo:");
		lblCodigo.setBounds(309, 21, 46, 14);
		contentPane.add(lblCodigo);
		
		txtCodigo = new JTextField();
		txtCodigo.setBounds(309, 47, 162, 20);
		txtCodigo.addFocusListener(getControlador());
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JLabel lblStock = new JLabel("Stock:");
		lblStock.setBounds(309, 91, 46, 14);
		contentPane.add(lblStock);
		
		txtStock = new JTextField();
		txtStock.setBounds(309, 116, 162, 20);
		txtStock.addFocusListener(getControlador());
		contentPane.add(txtStock);
		txtStock.setColumns(10);
		
		JLabel lblPrecio = new JLabel("Precio:");
		lblPrecio.setBounds(309, 163, 46, 14);
		contentPane.add(lblPrecio);
		
		txtPrecio = new JTextField();
		txtPrecio.setBounds(309, 188, 162, 20);
		txtPrecio.addFocusListener(getControlador());
		contentPane.add(txtPrecio);
		txtPrecio.setColumns(10);
		
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(309, 233, 86, 14);
		contentPane.add(lblDescripcion);
		
		JLabel lblMarca = new JLabel("Marca:");
		lblMarca.setBounds(28, 91, 46, 14);
		contentPane.add(lblMarca);
		
		cBoxMarca = new JComboBox();
		this.cargarMarcas();
		cBoxMarca.setBounds(28, 115, 152, 22);
		contentPane.add(cBoxMarca);
		
		JLabel lblCategoria = new JLabel("Categor�a:");
		lblCategoria.setBounds(28, 182, 58, 14);
		contentPane.add(lblCategoria);
		
		cBoxCategoria = new JComboBox();
		this.cargarCategorias();//metodo insertar categorias
		cBoxCategoria.setBounds(28, 207, 152, 22);
		contentPane.add(cBoxCategoria);
		
		JLabel lblCaducidad = new JLabel("\u00BFTiene Caducidad?");
		lblCaducidad.setBounds(28, 264, 134, 14);
		contentPane.add(lblCaducidad);
		
		rdbtnSi = new JRadioButton("Si");
		rdbtnSi.setBounds(28, 285, 46, 23);
		rdbtnSi.addChangeListener(controlador);
		contentPane.add(rdbtnSi);
		
		rdbtnNo = new JRadioButton("No");
		rdbtnNo.setBounds(76, 285, 46, 23);
		rdbtnNo.addChangeListener(controlador);
		contentPane.add(rdbtnNo);
		
		botonesCad.add(rdbtnSi);
		botonesCad.add(rdbtnNo);
		
		JLabel lblFechaCad = new JLabel("Fecha Caducidad:");
		lblFechaCad.setBounds(28, 315, 120, 14);
		contentPane.add(lblFechaCad);
		
		
		txtFechaCad = new JTextField();
		TextPrompt placeholder = new TextPrompt("dd/mm/aaaa", txtFechaCad);
		txtFechaCad.setEnabled(false);
		txtFechaCad.setBounds(28, 340, 134, 20);
		contentPane.add(txtFechaCad);
		txtFechaCad.setColumns(10);
		
		btnImagen = new JButton("Cargar Foto");
		btnImagen.setBounds(156, 419, 107, 23);
		btnImagen.addActionListener(controlador);
		contentPane.add(btnImagen);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(309, 258, 254, 134);
		contentPane.add(scrollPane);
		
		textAreaDescripcion = new JTextArea();
		textAreaDescripcion.setWrapStyleWord(true);
		textAreaDescripcion.setLineWrap(true);
		textAreaDescripcion.addFocusListener(getControlador());
		scrollPane.setViewportView(textAreaDescripcion);
		
		ImageIcon btnnuevo= new ImageIcon(new ImageIcon("img/botonnuevo.png").getImage());
		ImageIcon btnmodificar= new ImageIcon(new ImageIcon("img/botonmodificar.png").getImage());
		ImageIcon btneliminar= new ImageIcon(new ImageIcon("img/botoneliminar.png").getImage());
		
		btnNuevaMarca = new JButton("");
		btnNuevaMarca.setToolTipText("Nueva Marca");
		btnNuevaMarca.addActionListener(getControlador());
		btnNuevaMarca.setBounds(28, 148, 23, 23);
		btnNuevaMarca.setIcon(btnnuevo);
		contentPane.add(btnNuevaMarca);
		
		btnModificarMarca = new JButton("");
		btnModificarMarca.setToolTipText("Modificar Marca");
		btnModificarMarca.addActionListener(getControlador());
		btnModificarMarca.setBounds(61, 148, 23, 23);
		btnModificarMarca.setIcon(btnmodificar);
		contentPane.add(btnModificarMarca);
		
		btnBorrarMarca = new JButton("");
		btnBorrarMarca.setToolTipText("Eliminar Marca");
		btnBorrarMarca.addActionListener(getControlador());
		btnBorrarMarca.setBounds(94, 148, 23, 23);
		btnBorrarMarca.setIcon(btneliminar);
		contentPane.add(btnBorrarMarca);
		
		btnNuevaCategoria = new JButton("");
		btnNuevaCategoria.setToolTipText("Nueva Categor\u00EDa");
		btnNuevaCategoria.addActionListener(getControlador());
		btnNuevaCategoria.setBounds(28, 233, 23, 23);
		btnNuevaCategoria.setIcon(btnnuevo);
		contentPane.add(btnNuevaCategoria);
		
		btnModificarCategoria = new JButton("");
		btnModificarCategoria.setToolTipText("Modificar Categor\u00EDa");
		btnModificarCategoria.addActionListener(getControlador());
		btnModificarCategoria.setBounds(63, 233, 23, 23);
		btnModificarCategoria.setIcon(btnmodificar);
		contentPane.add(btnModificarCategoria);
		
		btnBorrarCategoria = new JButton("");
		btnBorrarCategoria.setToolTipText("Eliminar Categor\u00EDa");
		btnBorrarCategoria.addActionListener(getControlador());
		btnBorrarCategoria.setBounds(94, 233, 23, 23);
		btnBorrarCategoria.setIcon(btneliminar);
		contentPane.add(btnBorrarCategoria);
		
		lblverFoto = new JLabel();
		lblverFoto.setBounds(28, 396, 107, 77);
		lblverFoto.setIcon(new ImageIcon(new ImageIcon("img/imagendefault.png").getImage().getScaledInstance
   			 (getLblverFoto().getWidth(),getLblverFoto().getHeight(),Image.SCALE_DEFAULT)));
		contentPane.add(lblverFoto);
		
		lblAstCat = new JLabel("*");
		lblAstCat.setForeground(Color.RED);
		lblAstCat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstCat.setBounds(190, 211, 23, 14);
		contentPane.add(lblAstCat);
		
		lblAstMarc = new JLabel("*");
		lblAstMarc.setForeground(Color.RED);
		lblAstMarc.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstMarc.setBounds(190, 117, 23, 14);
		contentPane.add(lblAstMarc);
		
		lblAstNomb = new JLabel("*");
		lblAstNomb.setForeground(Color.RED);
		lblAstNomb.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstNomb.setBounds(190, 50, 23, 14);
		contentPane.add(lblAstNomb);
		
		lblAstCod = new JLabel("*");
		lblAstCod.setForeground(Color.RED);
		lblAstCod.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstCod.setBounds(481, 50, 23, 14);
		contentPane.add(lblAstCod);
		
		lblAstStock = new JLabel("*");
		lblAstStock.setForeground(Color.RED);
		lblAstStock.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstStock.setBounds(481, 117, 23, 14);
		contentPane.add(lblAstStock);
		
		lblAstPrec = new JLabel("*");
		lblAstPrec.setForeground(Color.RED);
		lblAstPrec.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstPrec.setBounds(481, 189, 23, 14);
		contentPane.add(lblAstPrec);
		
		lblAstDescr = new JLabel("*");
		lblAstDescr.setHorizontalAlignment(SwingConstants.CENTER);
		lblAstDescr.setForeground(Color.RED);
		lblAstDescr.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstDescr.setBounds(388, 231, 23, 14);
		contentPane.add(lblAstDescr);
		
		lblAstCampoObl = new JLabel("* Campo Obligatorio");
		lblAstCampoObl.setForeground(Color.RED);
		lblAstCampoObl.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAstCampoObl.setBounds(309, 403, 144, 20);
		contentPane.add(lblAstCampoObl);
		
		lblAstSeleccion = new JLabel("*");
		lblAstSeleccion.setHorizontalAlignment(SwingConstants.CENTER);
		lblAstSeleccion.setForeground(Color.RED);
		lblAstSeleccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstSeleccion.setBounds(135, 265, 23, 14);
		contentPane.add(lblAstSeleccion);
		
		//astericos
		this.getLblAstCat().setVisible(false);
		this.getLblAstCod().setVisible(false);
		this.getLblAstDescr().setVisible(false);
		this.getLblAstMarc().setVisible(false);
		this.getLblAstNomb().setVisible(false);
		this.getLblAstPrec().setVisible(false);
		this.getLblAstStock().setVisible(false);
		this.getLblAstSeleccion().setVisible(false);
		this.setLocationRelativeTo(null);
	}

	public JLabel getLblAstSeleccion() {
		return lblAstSeleccion;
	}

	public JLabel getLblAstCat() {
		return lblAstCat;
	}

	public void setLblAstCat(JLabel lblAstCat) {
		this.lblAstCat = lblAstCat;
	}

	public JLabel getLblAstMarc() {
		return lblAstMarc;
	}

	public void setLblAstMarc(JLabel lblAstMarc) {
		this.lblAstMarc = lblAstMarc;
	}

	public JLabel getLblAstNomb() {
		return lblAstNomb;
	}

	public void setLblAstNomb(JLabel lblAstNomb) {
		this.lblAstNomb = lblAstNomb;
	}

	public JLabel getLblAstCod() {
		return lblAstCod;
	}

	public void setLblAstCod(JLabel lblAstCod) {
		this.lblAstCod = lblAstCod;
	}

	public JLabel getLblAstStock() {
		return lblAstStock;
	}

	public void setLblAstStock(JLabel lblAstStock) {
		this.lblAstStock = lblAstStock;
	}

	public JLabel getLblAstPrec() {
		return lblAstPrec;
	}

	public void setLblAstPrec(JLabel lblAstPrec) {
		this.lblAstPrec = lblAstPrec;
	}

	public JLabel getLblAstDescr() {
		return lblAstDescr;
	}

	public void setLblAstDescr(JLabel lblAstDescr) {
		this.lblAstDescr = lblAstDescr;
	}

	public JLabel getLblAstCampoObl() {
		return lblAstCampoObl;
	}

	public void setLblAstCampoObl(JLabel lblAstCampoObl) {
		this.lblAstCampoObl = lblAstCampoObl;
	}


	public JLabel getLblverFoto() {
		return lblverFoto;
	}

	public void setLblverFoto(JLabel lblverFoto) {
		this.lblverFoto = lblverFoto;
	}

	public void cargarCategorias() {
		ArrayList<Categoria> categorias= new CategoriaDAO().getAll();
		for (Categoria categoria : categorias) {
			if (categoria.getEstado_categoria()==true) {
				this.getcBoxCategoria().addItem(categoria.getNombre_categoria());
			}
		}
	}
	
	public void cargarMarcas() {
		ArrayList<Marca> marcas= new MarcaDAO().getAll();
		for (Marca marca : marcas) {
			if (marca.getEstado_marca()==true) {
				this.getcBoxMarca().addItem(marca.getNombre_marca());
			}
		}
	}

	public JComboBox getcBoxMarca() {
		return cBoxMarca;
	}



	public void setcBoxMarca(JComboBox cBoxMarca) {
		this.cBoxMarca = cBoxMarca;
	}



	public JComboBox getcBoxCategoria() {
		return cBoxCategoria;
	}



	public void setcBoxCategoria(JComboBox cBoxCategoria) {
		this.cBoxCategoria = cBoxCategoria;
	}



	public JTextArea getTextAreaDescripcion() {
		return textAreaDescripcion;
	}



	public void setTextAreaDescripcion(JTextArea textAreaDescripcion) {
		this.textAreaDescripcion = textAreaDescripcion;
	}



	public JRadioButton getRdbtnSi() {
		return rdbtnSi;
	}



	public JRadioButton getRdbtnNo() {
		return rdbtnNo;
	}



	public JTextField getTxtNombre() {
		return txtNombre;
	}



	public JTextField getTxtCodigo() {
		return txtCodigo;
	}



	public JTextField getTxtStock() {
		return txtStock;
	}



	public JTextField getTxtPrecio() {
		return txtPrecio;
	}



	public JTextField getTxtFechaCad() {
		return txtFechaCad;
	}



	public JButton getBtnNuevaMarca() {
		return btnNuevaMarca;
	}



	public JButton getBtnModificarMarca() {
		return btnModificarMarca;
	}



	public JButton getBtnBorrarMarca() {
		return btnBorrarMarca;
	}



	public JButton getBtnNuevaCategoria() {
		return btnNuevaCategoria;
	}



	public JButton getBtnModificarCategoria() {
		return btnModificarCategoria;
	}



	public JButton getBtnBorrarCategoria() {
		return btnBorrarCategoria;
	}



	public JButton getBtnCancelar() {
		return btnCancelar;
	}



	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}



	public JButton getBtnImagen() {
		
		
		return btnImagen;
	}



	public ControladorVistaNuevoProducto getControlador() {
		return controlador;
	}


	public void setControlador(ControladorVistaNuevoProducto controlador) {
		this.controlador = controlador;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public void setTxtCodigo(JTextField txtCodigo) {
		this.txtCodigo = txtCodigo;
	}

	public void setTxtStock(JTextField txtStock) {
		this.txtStock = txtStock;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}

	public void setTxtFechaCad(JTextField txtFechaCad) {
		this.txtFechaCad = txtFechaCad;
	}

	public void setRdbtnSi(JRadioButton rdbtnSi) {
		this.rdbtnSi = rdbtnSi;
	}

	public void setRdbtnNo(JRadioButton rdbtnNo) {
		this.rdbtnNo = rdbtnNo;
	}
	
}
