package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaReporte;
import modelo.TextPrompt;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;

public class VistaReporte extends JFrame {

	private JPanel contentPane;
	private JTextField txtFechaInicio;
	private JTextField txtFechaFin;
	private ControladorVistaReporte controlador;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JLabel lblAdvertencia;

	public VistaReporte(ControladorVistaReporte controlador) {
		this.setControlador(controlador);
		setTitle("Reporte Productos Caducados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 405, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//TextPrompt placeholder = new TextPrompt("dd/mm/aaaa", txtFechaCad);
		JLabel lblImagen = new JLabel("");
		lblImagen.setBounds(26, 42, 157, 160);
		lblImagen.setIcon(new ImageIcon(new ImageIcon("img/calendario.png").getImage().getScaledInstance
	   			 (lblImagen.getWidth(),lblImagen.getHeight(),Image.SCALE_DEFAULT)));
		contentPane.add(lblImagen);
		
		JLabel lblNewLabel = new JLabel("Fecha Inicio:");
		lblNewLabel.setBounds(219, 42, 145, 14);
		contentPane.add(lblNewLabel);
		
		txtFechaInicio = new JTextField();
		txtFechaInicio.setBounds(219, 67, 145, 20);
		contentPane.add(txtFechaInicio);
		txtFechaInicio.setColumns(10);
		TextPrompt placeholder = new TextPrompt("dd/mm/aaaa", txtFechaInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha Fin:");
		lblFechaFin.setBounds(219, 118, 145, 14);
		contentPane.add(lblFechaFin);
		
		txtFechaFin = new JTextField();
		txtFechaFin.setColumns(10);
		txtFechaFin.setBounds(219, 143, 145, 20);
		contentPane.add(txtFechaFin);
		TextPrompt place2 = new TextPrompt("dd/mm/aaaa", txtFechaFin);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(197, 227, 89, 23);
		btnAceptar.addActionListener(getControlador());
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(290, 227, 89, 23);
		btnCancelar.addActionListener(getControlador());
		contentPane.add(btnCancelar);
		
		lblAdvertencia = new JLabel("La fecha debe ser menor a la fecha actual*");
		lblAdvertencia.setForeground(new Color(165, 42, 42));
		lblAdvertencia.setBounds(83, 17, 266, 14);
		lblAdvertencia.setVisible(false);
		contentPane.add(lblAdvertencia);
		
		this.setLocationRelativeTo(null);
	}

	public JLabel getLblAdvertencia() {
		return lblAdvertencia;
	}

	public ControladorVistaReporte getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVistaReporte controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public JTextField getTxtFechaInicio() {
		return txtFechaInicio;
	}

	public JTextField getTxtFechaFin() {
		return txtFechaFin;
	}
	
}
