package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaLogin;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.Font;

public class VistaLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtDni;
	private JPasswordField password;
	private JLabel lblImagenLogin;
	private JButton btnIniciar;
	private JButton btnCancelar;
	private ControladorVistaLogin controlador;


	
	public VistaLogin(ControladorVistaLogin controlador) {
		setTitle("InventoryControl - Login");
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 495, 323);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblImagenLogin = new JLabel("");
		lblImagenLogin.setBackground(Color.WHITE);
		lblImagenLogin.setBounds(25, 65, 211, 171);
		ImageIcon imagenlogin= new ImageIcon(new ImageIcon("img/login.png").getImage());
		lblImagenLogin.setIcon(imagenlogin);
		contentPane.add(lblImagenLogin);
		
		JLabel lblNewLabel_1 = new JLabel("LOG IN");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(153, 19, 154, 35);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblDni = new JLabel("DNI: ");
		lblDni.setBounds(246, 91, 46, 14);
		contentPane.add(lblDni);
		
		JLabel lblContrasenia = new JLabel("Contrase\u00F1a: ");
		lblContrasenia.setBounds(246, 157, 88, 14);
		contentPane.add(lblContrasenia);
		
		txtDni = new JTextField();
		txtDni.setBounds(337, 88, 132, 20);
		contentPane.add(txtDni);
		txtDni.setColumns(10);
		
		btnIniciar = new JButton("Iniciar");
		btnIniciar.setBounds(246, 235, 89, 23);
		contentPane.add(btnIniciar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(356, 235, 89, 23);
		contentPane.add(btnCancelar);
		
		password = new JPasswordField();
		password.setBounds(337, 154, 132, 20);
		contentPane.add(password);
		password.addKeyListener(getControlador());
		
		this.setLocationRelativeTo(null);
		this.getBtnIniciar().addActionListener(getControlador());
		this.getBtnCancelar().addActionListener(getControlador());
	}
	public ControladorVistaLogin getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVistaLogin controlador) {
		this.controlador = controlador;
	}
	public JButton getBtnIniciar() {
		return btnIniciar;
	}
	public void setBtnIniciar(JButton btnIniciar) {
		this.btnIniciar = btnIniciar;
	}
	public JButton getBtnCancelar() {
		return btnCancelar;
	}
	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
	public JTextField getTxtDni() {
		return txtDni;
	}
	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}
	public JPasswordField getPassword() {
		return password;
	}
	public void setPassword(JPasswordField password) {
		this.password = password;
	}
	
	
	
}
