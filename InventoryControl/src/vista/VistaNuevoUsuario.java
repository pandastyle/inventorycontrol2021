package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorNuevoUsuario;
import modelo.Sector;
import modelo.SectorDAO;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class VistaNuevoUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JTextField txtTelefono;
	private JPasswordField txtPass;
	private JPasswordField txtConfirPass;
	private JButton btnNuevoSector;
	private JButton btnModificarSec;
	private JButton btnEliminarSec;
	private JButton btnRegistrar;
	private JButton btnCancelar;
	private ControladorNuevoUsuario controlador;
	private JComboBox boxSector;
	private JComboBox boxTipoUsuario;
	private JLabel lblAstSector;
	private JLabel lblAstRol;
	private JLabel lblAstConfPass;
	private JLabel lblAstPass;
	private JLabel lblAstUsuario;
	private JLabel lblAstNomb;
	private JLabel lblAstApe;
	private JLabel lblAstCampoObl;

	public VistaNuevoUsuario(ControladorNuevoUsuario controlador) {
		this.setControlador(controlador);
		setTitle("InventoryControl - Nuevo Usuario");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 470, 395);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(28, 28, 64, 14);
		contentPane.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(28, 48, 144, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		txtUsuario.addFocusListener(controlador);
		
		JLabel lblContrasenia = new JLabel("Contrase\u00F1a:");
		lblContrasenia.setBounds(28, 80, 113, 14);
		contentPane.add(lblContrasenia);
		
		JLabel lblConfirmacion = new JLabel("Confirmar Contrase\u00F1a:");
		lblConfirmacion.setBounds(28, 133, 144, 14);
		contentPane.add(lblConfirmacion);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(279, 48, 144, 20);
		contentPane.add(txtNombre);
		txtNombre.addFocusListener(controlador);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBounds(279, 28, 64, 14);
		contentPane.add(lblNombre);
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(279, 100, 144, 20);
		contentPane.add(txtApellido);
		txtApellido.addFocusListener(controlador);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(279, 80, 64, 14);
		contentPane.add(lblApellido);
		
		txtCorreo = new JTextField();
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(279, 153, 144, 20);
		contentPane.add(txtCorreo);
		
		JLabel lblCorreo = new JLabel("Correo:");
		lblCorreo.setBounds(279, 133, 64, 14);
		contentPane.add(lblCorreo);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(279, 204, 144, 20);
		contentPane.add(txtTelefono);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(279, 184, 64, 14);
		contentPane.add(lblTelefono);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(28, 105, 144, 20);
		contentPane.add(txtPass);
		txtPass.addFocusListener(controlador);
		
		txtConfirPass = new JPasswordField();
		txtConfirPass.setBounds(28, 153, 144, 20);
		contentPane.add(txtConfirPass);
		txtConfirPass.addFocusListener(controlador);
		
		String[] contenido= {"Seleccionar","Administrador Total","Administrador Empleados","Empleado"};
		boxTipoUsuario = new JComboBox(contenido);
		boxTipoUsuario.addItemListener(getControlador());
		boxTipoUsuario.setBounds(28, 203, 144, 20);
		contentPane.add(boxTipoUsuario);
		boxTipoUsuario.addFocusListener(controlador);
		
		JLabel lblTipoDeUsuario = new JLabel("Rol:");
		lblTipoDeUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		lblTipoDeUsuario.setBounds(28, 184, 144, 14);
		contentPane.add(lblTipoDeUsuario);
		
		JLabel lblSector = new JLabel("Sector:");
		lblSector.setHorizontalAlignment(SwingConstants.LEFT);
		lblSector.setBounds(28, 232, 144, 14);
		contentPane.add(lblSector);
		
		boxSector = new JComboBox();
		boxSector.addItem("SELECCIONAR");
		this.cargarSector();
		boxSector.setEnabled(false);
		boxSector.setBounds(28, 251, 144, 20);
		contentPane.add(boxSector);
		boxSector.addFocusListener(controlador);
		
		ImageIcon btnnuevo= new ImageIcon(new ImageIcon("img/botonnuevo.png").getImage());
		ImageIcon btnmodificar= new ImageIcon(new ImageIcon("img/botonmodificar.png").getImage());
		ImageIcon btneliminar= new ImageIcon(new ImageIcon("img/botoneliminar.png").getImage());
		
		btnNuevoSector = new JButton(btnnuevo);
		btnNuevoSector.setToolTipText("Nuevo Sector");
		btnNuevoSector.addActionListener(getControlador());
		btnNuevoSector.setBounds(28, 273, 23, 23);
		btnNuevoSector.setEnabled(false);
		contentPane.add(btnNuevoSector);
		
		btnModificarSec = new JButton(btnmodificar);
		btnModificarSec.setToolTipText("Modificar Sector");
		btnModificarSec.addActionListener(getControlador());
		btnModificarSec.setBounds(61, 273, 23, 23);
		btnModificarSec.setEnabled(false);
		contentPane.add(btnModificarSec);
		
		btnEliminarSec = new JButton(btneliminar);
		btnEliminarSec.setToolTipText("Eliminar Sector");
		btnEliminarSec.addActionListener(getControlador());
		btnEliminarSec.setEnabled(false);
		btnEliminarSec.setBounds(94, 273, 23, 23);
		contentPane.add(btnEliminarSec);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(getControlador());
		btnRegistrar.setBounds(254, 311, 89, 23);
		contentPane.add(btnRegistrar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(getControlador());
		btnCancelar.setBounds(353, 311, 89, 23);
		contentPane.add(btnCancelar);
		
		lblAstRol = new JLabel("*");
		lblAstRol.setForeground(Color.RED);
		lblAstRol.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstRol.setBounds(182, 205, 23, 14);
		contentPane.add(lblAstRol);
		lblAstRol.addFocusListener(controlador);
		lblAstRol.setVisible(false);
		
		lblAstConfPass = new JLabel("*");
		lblAstConfPass.setForeground(Color.RED);
		lblAstConfPass.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstConfPass.setBounds(182, 156, 23, 14);
		contentPane.add(lblAstConfPass);
		lblAstConfPass.addFocusListener(controlador);
		lblAstConfPass.setVisible(false);
		
		
		lblAstPass = new JLabel("*");
		lblAstPass.setForeground(Color.RED);
		lblAstPass.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstPass.setBounds(182, 106, 23, 14);
		contentPane.add(lblAstPass);
		lblAstPass.addFocusListener(controlador);
		lblAstPass.setVisible(false);
		
		lblAstUsuario = new JLabel("*");
		lblAstUsuario.setForeground(Color.RED);
		lblAstUsuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstUsuario.setBounds(185, 51, 23, 14);
		contentPane.add(lblAstUsuario);
		lblAstUsuario.addFocusListener(controlador);
		lblAstUsuario.setVisible(false);
		
		lblAstNomb = new JLabel("*");
		lblAstNomb.setForeground(Color.RED);
		lblAstNomb.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstNomb.setBounds(433, 51, 23, 14);
		contentPane.add(lblAstNomb);
		lblAstNomb.addFocusListener(controlador);
		lblAstNomb.setVisible(false);
		
		lblAstApe = new JLabel("*");
		lblAstApe.setForeground(Color.RED);
		lblAstApe.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstApe.setBounds(433, 103, 23, 14);
		contentPane.add(lblAstApe);
		lblAstApe.addFocusListener(controlador);
		lblAstApe.setVisible(false);
		
		lblAstSector = new JLabel("*");
		lblAstSector.setForeground(Color.RED);
		lblAstSector.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAstSector.setBounds(182, 254, 23, 14);
		contentPane.add(lblAstSector);
		lblAstSector.addFocusListener(controlador);
		lblAstSector.setVisible(false);
		
		lblAstCampoObl = new JLabel("* Campo Obligatorio");
		lblAstCampoObl.setForeground(Color.RED);
		lblAstCampoObl.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAstCampoObl.setBounds(28, 311, 144, 20);
		contentPane.add(lblAstCampoObl);
		this.setLocationRelativeTo(null);
	}
	public JLabel getLblAstSector() {
		return lblAstSector;
	}
	public void setLblAstSector(JLabel lblAstSector) {
		this.lblAstSector = lblAstSector;
	}
	public JLabel getLblAstRol() {
		return lblAstRol;
	}
	public void setLblAstRol(JLabel lblAstRol) {
		this.lblAstRol = lblAstRol;
	}
	public JLabel getLblAstCampoObl() {
		return lblAstCampoObl;
	}
	public void setLblAstCampoObl(JLabel lblAstCampoObl) {
		this.lblAstCampoObl = lblAstCampoObl;
	}
	public JLabel getLblAstConfPass() {
		return lblAstConfPass;
	}
	public void setLblAstConfPass(JLabel lblAstConfPass) {
		this.lblAstConfPass = lblAstConfPass;
	}
	public JLabel getLblAstPass() {
		return lblAstPass;
	}
	public void setLblAstPass(JLabel lblAstPass) {
		this.lblAstPass = lblAstPass;
	}
	public JLabel getLblAstUsuario() {
		return lblAstUsuario;
	}
	public void setLblAstUsuario(JLabel lblAstUsuario) {
		this.lblAstUsuario = lblAstUsuario;
	}
	public JLabel getLblAstNomb() {
		return lblAstNomb;
	}
	public void setLblAstNomb(JLabel lblAstNomb) {
		this.lblAstNomb = lblAstNomb;
	}
	public JLabel getLblAstApe() {
		return lblAstApe;
	}
	public void setLblAstApe(JLabel lblAstApe) {
		this.lblAstApe = lblAstApe;
	}
	public void cargarSector() {
		ArrayList<Sector> sectores= new SectorDAO().getAll();
		for (Sector sector : sectores) {
			if (sector.getEstado_sector()==true) {
				this.getBoxSector().addItem(sector.getNombre_sector());
			}	
		}
	}

	public ControladorNuevoUsuario getControlador() {
		return controlador;
	}

	public void setControlador(ControladorNuevoUsuario controlador) {
		this.controlador = controlador;
	}
	public JTextField getTxtUsuario() {
		return txtUsuario;
	}
	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}
	public JTextField getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}
	public JTextField getTxtApellido() {
		return txtApellido;
	}
	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}
	public JTextField getTxtCorreo() {
		return txtCorreo;
	}
	public void setTxtCorreo(JTextField txtCorreo) {
		this.txtCorreo = txtCorreo;
	}
	public JTextField getTxtTelefono() {
		return txtTelefono;
	}
	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}
	public JPasswordField getTxtPass() {
		return txtPass;
	}
	public void setTxtPass(JPasswordField txtPass) {
		this.txtPass = txtPass;
	}
	public JPasswordField getTxtConfirPass() {
		return txtConfirPass;
	}
	public void setTxtConfirPass(JPasswordField txtConfirPass) {
		this.txtConfirPass = txtConfirPass;
	}
	public JButton getBtnNuevoSector() {
		return btnNuevoSector;
	}
	public void setBtnNuevoSector(JButton btnNuevoSector) {
		this.btnNuevoSector = btnNuevoSector;
	}
	public JButton getBtnModificarSec() {
		return btnModificarSec;
	}
	public void setBtnModificarSec(JButton btnModificarSec) {
		this.btnModificarSec = btnModificarSec;
	}
	public JButton getBtnEliminarSec() {
		return btnEliminarSec;
	}
	public void setBtnEliminarSec(JButton btnEliminarSec) {
		this.btnEliminarSec = btnEliminarSec;
	}
	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}
	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}
	public JButton getBtnCancelar() {
		return btnCancelar;
	}
	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
	public JComboBox getBoxSector() {
		return boxSector;
	}
	public void setBoxSector(JComboBox boxSector) {
		this.boxSector = boxSector;
	}
	public JComboBox getBoxTipoUsuario() {
		return boxTipoUsuario;
	}
	public void setBoxTipoUsuario(JComboBox boxTipoUsuario) {
		this.boxTipoUsuario = boxTipoUsuario;
	}
}
