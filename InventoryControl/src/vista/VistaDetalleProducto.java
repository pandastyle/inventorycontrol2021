package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaDetalleProducto;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Font;

public class VistaDetalleProducto extends JFrame {

	private JPanel contentPane;
	private JLabel lblFoto;
	private JLabel lblNombreProducto;
	private JLabel lblMarca;
	private JLabel lblCategoria;
	private JLabel lblPrecio;
	private JLabel lblStock;
	private JLabel lblfechaCaducidad;
	private ControladorVistaDetalleProducto controlador;
	private JTextArea textArea;

	public VistaDetalleProducto(ControladorVistaDetalleProducto controlador) {
		this.setControlador(controlador);
		setTitle("InventoryControl - DetalleProducto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 637, 336);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.menu);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblFoto = new JLabel("");
		lblFoto.setForeground(Color.WHITE);
		lblFoto.setBackground(Color.WHITE);
		lblFoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblFoto.setBounds(39, 43, 191, 208);
		ImageIcon imagendefault = new ImageIcon(new ImageIcon("img/imagendefault.png").getImage());
		lblFoto.setIcon(imagendefault);
		contentPane.add(lblFoto);
		
		lblNombreProducto = new JLabel("[NOMBRE PRODUCTO]");
		lblNombreProducto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNombreProducto.setBounds(257, 34, 191, 23);
		contentPane.add(lblNombreProducto);
		
		lblMarca = new JLabel("[Marca]");
		lblMarca.setBounds(257, 61, 107, 14);
		contentPane.add(lblMarca);
		
		lblCategoria = new JLabel("[Categoria]");
		lblCategoria.setBounds(374, 61, 160, 14);
		contentPane.add(lblCategoria);
		
		lblStock = new JLabel("[Stock]");
		lblStock.setBounds(257, 80, 107, 14);
		contentPane.add(lblStock);
		
		lblPrecio = new JLabel("[Precio]");
		lblPrecio.setBounds(374, 80, 146, 14);
		contentPane.add(lblPrecio);
		
		lblfechaCaducidad = new JLabel("");
		lblfechaCaducidad.setBounds(257, 242, 128, 14);
		contentPane.add(lblfechaCaducidad);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(522, 263, 89, 23);
		btnVolver.addActionListener(getControlador());
		contentPane.add(btnVolver);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(257, 105, 354, 138);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		this.setLocationRelativeTo(null);
	}

	public ControladorVistaDetalleProducto getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVistaDetalleProducto controlador) {
		this.controlador = controlador;
	}

	public JLabel getLblFoto() {
		return lblFoto;
	}

	public JLabel getLblNombreProducto() {
		return lblNombreProducto;
	}

	public JLabel getLblMarca() {
		return lblMarca;
	}

	public JLabel getLblCategoria() {
		return lblCategoria;
	}

	public JLabel getLblPrecio() {
		return lblPrecio;
	}

	public JLabel getLblStock() {
		return lblStock;
	}



	public JLabel getLblfechaCaducidad() {
		return lblfechaCaducidad;
	}

	public JTextArea getTextArea() {
		return textArea;
	}
}
