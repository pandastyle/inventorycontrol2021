package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controlador.ControladorVistaGeneral;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class VistaPrincipalGeneral extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel modeloTabla;
	
	private JButton btnInformes;
	private JButton btnProductos;
	private JTextField txtBuscar;
	private JPanel panelSuperior;
	private JPanel panelProducto;
	private JButton btnNuevoProducto;
	private JButton btnModificarProducto;
	private JButton btnEliminarProducto;
	private JButton btnDetalleProducto;
	private JPanel panelDefault;

	private JButton btnCerrarSesion;
	private ControladorVistaGeneral controlador;
	private JPanel panelInformes;
	private JButton btnInformesCaducado;
	private JButton btnInformeGrafico;
	private JButton btnInformeGanancias;
	private JButton btncuenta;
	private JTextField txtBuscarEmpleado;

	public VistaPrincipalGeneral(ControladorVistaGeneral controlador) {
		this.setControlador(controlador);
		setTitle("InventoryControl");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 456);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelSuperior = new JPanel();
		panelSuperior.setBackground(Color.BLACK);
		panelSuperior.setBounds(0, 0, 884, 69);
		contentPane.add(panelSuperior);
		panelSuperior.setLayout(null);
		
		JLabel lblTitulo = new JLabel("InventoryControl");
		lblTitulo.setFont(new Font("Impacted 2.0", Font.PLAIN, 30));
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setBounds(10, 11, 290, 47);
		panelSuperior.add(lblTitulo);
		
		btnInformes = new JButton("INFORMES");
		btnInformes.setBackground(Color.WHITE);
		btnInformes.setBounds(430, 20, 122, 35);
		panelSuperior.add(btnInformes);
		
		btnProductos = new JButton("PRODUCTOS");
		btnProductos.setBackground(Color.WHITE);
		btnProductos.setBounds(562, 20, 122, 35);
		panelSuperior.add(btnProductos);
		
		
		ImageIcon cuenta= new ImageIcon(new ImageIcon("img/micuenta.png").getImage());
		
		btncuenta = new JButton("");
		btncuenta.setBounds(711, 20, 107, 37);
		btncuenta.setIcon(cuenta);
		
		panelSuperior.add(btncuenta);
		
		ImageIcon cerrarsesion= new ImageIcon(new ImageIcon("img/cerrarsesion.png").getImage());
		btnCerrarSesion = new JButton("");
		btnCerrarSesion.setBackground(Color.BLACK);
		btnCerrarSesion.setBounds(818, 20, 37, 37);
		
		btnCerrarSesion.setIcon(cerrarsesion);
	
		panelSuperior.add(btnCerrarSesion);
		
		panelDefault = new JPanel();
		panelDefault.setBackground(Color.GRAY);
		panelDefault.setBounds(683, 68, 201, 349);
		contentPane.add(panelDefault);
		panelDefault.setLayout(null);
		
		panelProducto = new JPanel();
		
		panelProducto.setBackground(Color.GRAY);
		panelProducto.setBounds(0, 24, 201, 314);
		panelDefault.add(panelProducto);
		
		panelProducto.setLayout(null);
		
		btnNuevoProducto = new JButton("Nuevo");
		btnNuevoProducto.setBounds(33, 38, 137, 46);
		btnNuevoProducto.setBackground(new Color(240, 248, 255));
		panelProducto.add(btnNuevoProducto);
		
		btnModificarProducto = new JButton("Modificar");
		btnModificarProducto.setBounds(33, 95, 137, 46);
		btnModificarProducto.setBackground(new Color(240, 248, 255));
		panelProducto.add(btnModificarProducto);
		
		btnEliminarProducto = new JButton("Eliminar");
		btnEliminarProducto.setBounds(33, 152, 137, 46);
		btnEliminarProducto.setBackground(new Color(240, 248, 255));
		panelProducto.add(btnEliminarProducto);
		
		btnDetalleProducto = new JButton("Detalle Producto");
		btnDetalleProducto.setBounds(33, 209, 137, 46);
		btnDetalleProducto.setBackground(new Color(240, 248, 255));
		panelProducto.add(btnDetalleProducto);
		
		panelInformes = new JPanel();
		panelInformes.setLayout(null);
		panelInformes.setBackground(Color.GRAY);
		panelInformes.setBounds(0, 24, 201, 314);
		panelDefault.add(panelInformes);
		
		btnInformesCaducado = new JButton("Caducidad");
		btnInformesCaducado.setBackground(new Color(240, 248, 255));
		btnInformesCaducado.setBounds(33, 38, 137, 46);
		panelInformes.add(btnInformesCaducado);
		
		btnInformeGrafico = new JButton("Grafico");
		btnInformeGrafico.setBackground(new Color(240, 248, 255));
		btnInformeGrafico.setBounds(33, 95, 137, 46);
		panelInformes.add(btnInformeGrafico);
		
		btnInformeGanancias = new JButton("Ganancias");
		btnInformeGanancias.setBackground(new Color(240, 248, 255));
		btnInformeGanancias.setBounds(33, 152, 137, 46);
		panelInformes.add(btnInformeGanancias);
		panelInformes.setVisible(false);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 116, 663, 290);
		contentPane.add(scrollPane);
		
		String cabecera[]= {"id","C�digo","Producto","Categor�a","Marca","Stock"};
		modeloTabla= new DefaultTableModel(null,cabecera) {
			@Override
			public boolean isCellEditable(int row, int column) {
			if (column==6) {
				return true;
			} else {
				return false;
			}
		}
		};
		
		table = new JTable(modeloTabla);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		table.setAutoCreateRowSorter(true);
		//oculta el id de la tabla
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
		table.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
	

		
		JLabel lblBuscar = new JLabel("Buscar:");
		lblBuscar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBuscar.setBounds(10, 80, 74, 25);
		contentPane.add(lblBuscar);
		
		txtBuscar = new JTextField();
		//txtBuscar.addFocusListener(getControlador());
		txtBuscar.setBounds(54, 85, 242, 20);
		contentPane.add(txtBuscar);
		txtBuscar.setColumns(10);
		
		txtBuscarEmpleado = new JTextField();
		txtBuscarEmpleado.setColumns(10);
		txtBuscarEmpleado.setBounds(54, 85, 242, 20);
		contentPane.add(txtBuscarEmpleado);
		txtBuscarEmpleado.setVisible(false);
		addWindowListener(this.getControlador());
		
		//BOTONES PRODUCTOS
		this.getBtnProductos().addActionListener(getControlador());
		this.getBtnNuevoProducto().addActionListener(getControlador());
		this.getBtnModificarProducto().addActionListener(getControlador());
		this.getBtnEliminarProducto().addActionListener(getControlador());
		this.getBtnDetalleProducto().addActionListener(getControlador());
		this.getBtnDetalleProducto().addFocusListener(getControlador());
		//BOTONES INFORMES
		this.getBtnInformes().addActionListener(getControlador());
		this.getBtnInformeGanancias().addActionListener(getControlador());
		this.getBtnInformeGrafico().addActionListener(getControlador());
		this.getBtnInformesCaducado().addActionListener(getControlador());
		this.getBtnInformes().addFocusListener(getControlador());
		
		//TABLA
		this.getTxtBuscar().addKeyListener(getControlador());
		this.getTxtBuscarEmpleado().addKeyListener(getControlador());
		this.getTable().getSelectionModel().addListSelectionListener(this.getControlador());;
		this.setLocationRelativeTo(null);
		this.getTable().addFocusListener(getControlador());
		//MENU
		this.getBtncuenta().addActionListener(getControlador());
		this.getBtnCerrarSesion().addActionListener(getControlador());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public JButton getBtnInformesCaducado() {
		return btnInformesCaducado;
	}

	public JButton getBtnInformeGrafico() {
		return btnInformeGrafico;
	}

	public JButton getBtnInformeGanancias() {
		return btnInformeGanancias;
	}

	public JPanel getPanelInformes() {
		return panelInformes;
	}

	public JButton getBtnCerrarSesion() {
		return btnCerrarSesion;
	}

	public void setBtnCerrarSesion(JButton btnCerrarSesion) {
		this.btnCerrarSesion = btnCerrarSesion;
	}

	public JPanel getPanelSuperior() {
		return panelSuperior;
	}

	public void setPanelSuperior(JPanel panelSuperior) {
		this.panelSuperior = panelSuperior;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}


	public JButton getBtnInformes() {
		return btnInformes;
	}

	public void setBtnInformes(JButton btnInformes) {
		this.btnInformes = btnInformes;
	}

	public JButton getBtnProductos() {
		return btnProductos;
	}

	public void setBtnProductos(JButton btnProductos) {
		this.btnProductos = btnProductos;
	}

	
	public JTextField getTxtBuscar() {
		return txtBuscar;
	}

	public void setTxtBuscar(JTextField txtBuscar) {
		this.txtBuscar = txtBuscar;
	}

	public JPanel getPanelProducto() {
		return panelProducto;
	}

	

	public JPanel getPanelDefault() {
		return panelDefault;
	}

	

	public JButton getBtnNuevoProducto() {
		return btnNuevoProducto;
	}

	public JButton getBtnModificarProducto() {
		return btnModificarProducto;
	}

	public JButton getBtnEliminarProducto() {
		return btnEliminarProducto;
	}

	public JButton getBtnDetalleProducto() {
		return btnDetalleProducto;
	}

	public ControladorVistaGeneral getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVistaGeneral controlador) {
		this.controlador = controlador;
	}

	public JButton getBtncuenta() {
		return btncuenta;
	}

	public JTextField getTxtBuscarEmpleado() {
		return txtBuscarEmpleado;
	}
	
}
