package vista;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controlador.ControladorVistaGeneral;
import controlador.ControladorVistaPrincipalAdministrador;

public class VistaPrincipalAdministrador extends VistaPrincipalGeneral {

	private ControladorVistaGeneral controlador;
    private JButton btnEmpleados;
	private JButton btnNuevoEmpleado;
	private JButton btnModificarEmpleado;
	private JButton btnEliminarEmpleado;
	private JPanel panelEmpleados;
	public VistaPrincipalAdministrador(ControladorVistaGeneral controlador) {
		super(controlador);
		btnEmpleados = new JButton("EMPLEADOS");
		btnEmpleados.setFocusPainted(false);
		btnEmpleados.addActionListener(getControlador());
		btnEmpleados.addFocusListener(getControlador());
		btnEmpleados.setBackground(Color.WHITE);
		btnEmpleados.setBounds(298, 20, 122, 35);
		this.getPanelSuperior().add(btnEmpleados);
		
		addWindowListener(this.getControlador());
		panelEmpleados = new JPanel();
		panelEmpleados.setBackground(Color.GRAY);
		panelEmpleados.setBounds(0, 24, 201, 314);
		this.getPanelDefault().add(panelEmpleados);
		panelEmpleados.setVisible(false);//se hace visible cuando se presiona el boton de empleados
		panelEmpleados.setLayout(null);
		
		btnNuevoEmpleado = new JButton("Nuevo Usuario");
		btnNuevoEmpleado.setBackground(new Color(240, 248, 255));
		btnNuevoEmpleado.setBounds(33, 38, 137, 46);
		panelEmpleados.add(btnNuevoEmpleado);
		
		btnModificarEmpleado = new JButton("Modificar");
		btnModificarEmpleado.setBackground(new Color(240, 248, 255));
		btnModificarEmpleado.setBounds(33, 95, 137, 46);
		panelEmpleados.add(btnModificarEmpleado);
		
		btnEliminarEmpleado = new JButton("Eliminar");
		btnEliminarEmpleado.setBackground(new Color(240, 248, 255));
		btnEliminarEmpleado.setBounds(33, 152, 137, 46);
		panelEmpleados.add(btnEliminarEmpleado);
		
		getBtnNuevoEmpleado().addActionListener(getControlador());
		getBtnModificarEmpleado().addActionListener(getControlador());
		getBtnEliminarEmpleado().addActionListener(getControlador());
	}
	
	public ControladorVistaGeneral getControlador() {
		return controlador;
	}
	public void setControlador(ControladorVistaGeneral controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnEmpleados() {
		return btnEmpleados;
	}

	public void setBtnEmpleados(JButton btnEmpleados) {
		this.btnEmpleados = btnEmpleados;
	}

	public JButton getBtnNuevoEmpleado() {
		return btnNuevoEmpleado;
	}

	public JButton getBtnModificarEmpleado() {
		return btnModificarEmpleado;
	}

	public JButton getBtnEliminarEmpleado() {
		return btnEliminarEmpleado;
	}

	public JPanel getPanelEmpleados() {
		return panelEmpleados;
	}
	
}
