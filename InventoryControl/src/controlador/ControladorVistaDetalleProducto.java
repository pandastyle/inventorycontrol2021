package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vista.VistaDetalleProducto;

public class ControladorVistaDetalleProducto implements ActionListener {
	private VistaDetalleProducto vista;
	
	

	public ControladorVistaDetalleProducto() {
	
		this.vista = new VistaDetalleProducto(this);
		this.getVista().setVisible(true);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		this.getVista().dispose();

	}



	public VistaDetalleProducto getVista() {
		return vista;
	}



	public void setVista(VistaDetalleProducto vista) {
		this.vista = vista;
	}

	
}
