package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import excepciones.ExcepcionLogin;
import excepciones.Mensajes;
import modelo.MD5;
import modelo.Usuario;
import modelo.UsuarioAdministradorDAO;
import vista.VistaLogin;
import vista.VistaNuevoProducto;

public class ControladorVistaLogin implements ActionListener, KeyListener{

	private VistaLogin vista;
	
	public ControladorVistaLogin() {
		this.vista=new VistaLogin(this);
		this.getVista().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	if (e.getSource().equals(getVista().getBtnCancelar())) {
		this.getVista().dispose();
	
	}
	
	if (e.getSource().equals(getVista().getBtnIniciar())) {
		try {
		Integer usuario= Integer.valueOf(this.getVista().getTxtDni().getText());
		String password= MD5.encriptar(new String(this.getVista().getPassword().getPassword()));
		Usuario usu= new UsuarioAdministradorDAO().logeo(usuario, password);
		if (usu!=null) {
			if (usu.getTipo_usuario().equals("Empleado")) {
				new ControladorVistaPrincipalEmpleado(usu);
				this.getVista().dispose();
			}else if (usu.getTipo_usuario().equals("Administrador Total")){
				new ControladorVistaPrincipalAdministrador(usu);
				this.getVista().dispose();
			}else {
				ControladorVistaPrincipalAdministrador controlador =new ControladorVistaPrincipalAdministrador(usu);
				controlador.getVistaprincipal().getBtnProductos().setEnabled(false);
				controlador.getVistaprincipal().getBtnProductos().setVisible(false);
				controlador.getVistaprincipal().getPanelProducto().setVisible(false);
				this.getVista().dispose();
			}
		}else {
			try {
			throw new ExcepcionLogin();
			}catch (ExcepcionLogin t) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.Login, "InventoryControl- Login", 1);
			}
		}
		}catch(NumberFormatException j) {
			JOptionPane.showMessageDialog(this.getVista(), "Ingrese un formato valido", "InventoryControl- Login", 1);
		}
		
				
			
	}
		
	}
	
	public VistaLogin getVista() {
		return vista;
	}



	public void setVista(VistaLogin vista) {
		this.vista = vista;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getSource().equals(getVista().getPassword())) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER ) {
				try {
					Integer usuario= Integer.valueOf(this.getVista().getTxtDni().getText());
					String password= MD5.encriptar(new String(this.getVista().getPassword().getPassword()));
					Usuario usu= new UsuarioAdministradorDAO().logeo(usuario, password);
					if (usu!=null) {
						if (usu.getTipo_usuario().equals("Empleado")) {
							new ControladorVistaPrincipalEmpleado(usu);
							this.getVista().dispose();
						}else if (usu.getTipo_usuario().equals("Administrador Total")){
							new ControladorVistaPrincipalAdministrador(usu);
							this.getVista().dispose();
						}else {
							ControladorVistaPrincipalAdministrador controlador =new ControladorVistaPrincipalAdministrador(usu);
							controlador.getVistaprincipal().getBtnProductos().setEnabled(false);
							controlador.getVistaprincipal().getBtnProductos().setVisible(false);
							controlador.getVistaprincipal().getPanelProducto().setVisible(false);
							this.getVista().dispose();
						}
					}else {
						try {
						throw new ExcepcionLogin();
						}catch (ExcepcionLogin t) {
							JOptionPane.showMessageDialog(this.getVista(), Mensajes.Login, "InventoryControl- Login", 1);
						}
					}
					}catch(NumberFormatException j) {
						JOptionPane.showMessageDialog(this.getVista(), "Ingrese un formato valido", "InventoryControl- Login", 1);
					}
			}
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}




	

}
