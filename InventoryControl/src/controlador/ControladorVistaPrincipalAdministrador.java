package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import modelo.ProductoDAO;
import modelo.Usuario;
import modelo.UsuarioAdministrador;
import modelo.UsuarioAdministradorDAO;
import modelo.UsuarioEmpleado;
import modelo.UsuarioEmpleadoDAO;
import vista.VistaPrincipalAdministrador;

public class ControladorVistaPrincipalAdministrador extends ControladorVistaGeneral  {
	private VistaPrincipalAdministrador vista;

	public ControladorVistaPrincipalAdministrador(Usuario usu) {
		//los seteos de la vista son dentro de los hijos
		super(usu);
		this.setVista(new VistaPrincipalAdministrador(this));
		this.setVistaprincipal(this.getVista());
		this.getVistaprincipal().setVisible(true);
		this.actualizarModeloTabla();
		super.habilitacionBotones(false);
	
		
	}
	
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		if (e.getSource().equals(getVista().getBtnEmpleados())) {
			this.getVistaprincipal().getPanelProducto().setVisible(false);
			this.getVistaprincipal().getPanelInformes().setVisible(false);
			this.getVista().getPanelEmpleados().setVisible(true);
			this.getVistaprincipal().getTxtBuscar().setVisible(false);
			this.getVistaprincipal().getTxtBuscarEmpleado().setVisible(true);
			this.getVista().getBtnEmpleados().setBorder(BorderFactory.createLineBorder(Color.GREEN));
			this.habilitacionBotonEmpleado(false);
			
			this.actualizarTablaEmpleados();

		}
		
		if (e.getSource().equals(getVista().getBtnNuevoEmpleado())) {//NUEVO EMPLEADO
			UsuarioAdministrador usu=null;
			new ControladorNuevoUsuario(usu);
		}
		
		if (e.getSource().equals(getVista().getBtnModificarEmpleado())) {//MODIFICAR USUARIO
			Integer idusu=	(Integer) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 0);
			String rol=(String) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 4);
			if (rol.equals("Empleado")) {
				UsuarioEmpleado usuEmp= new UsuarioEmpleadoDAO().get(idusu);
				new ControladorNuevoUsuario(usuEmp);
				
			}else {
				UsuarioAdministrador usuADM= new UsuarioAdministradorDAO().get(idusu);
				new ControladorNuevoUsuario(usuADM);
			}
		}
		
		if (e.getSource().equals(getVista().getBtnEliminarEmpleado())) {
			Integer idusu=	(Integer) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 0);
			String rol=(String) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 4);
			if (rol.equals("Empleado")) {
				UsuarioEmpleado usuEmp= new UsuarioEmpleadoDAO().get(idusu);
				Integer confirmacion=JOptionPane.showConfirmDialog(this.getVista(),"�Seguro que quiere eliminar a "+usuEmp.getNombre_usuario()+" "+usuEmp.getApellido_usuario()+"?", "Eliminar Usuario", 1, 2);
				if (confirmacion==0) {
					if (new UsuarioEmpleadoDAO().baja(usuEmp)) {
						JOptionPane.showMessageDialog(getVistaprincipal(), "Usuario Eliminado Exitosamente");	
					}
				}
				
			}else {
				UsuarioAdministrador usuADM= new UsuarioAdministradorDAO().get(idusu);
				Integer confirmacion=JOptionPane.showConfirmDialog(this.getVista(),"�Seguro que quiere eliminar a "+usuADM.getNombre_usuario()+" "+usuADM.getApellido_usuario()+"?", "Eliminar Usuario", 1, 2);
				if (confirmacion==0) {
					if (new UsuarioAdministradorDAO().baja(usuADM)) {
						JOptionPane.showMessageDialog(getVistaprincipal(), "Usuario Eliminado Exitosamente");	
					}
				}
				
			}
		}
		
	}
	
	public void habilitacionBotonEmpleado(Boolean estado) {
		this.getVista().getBtnModificarEmpleado().setEnabled(estado);
		this.getVista().getBtnEliminarEmpleado().setEnabled(estado);
	}
	
	public void actualizarTablaEmpleados() {//aplicar para sacar los dados de baja
		String cabecera[]= {"id","Usuario","Nombre Empleado","Correo","Rol","Sector"};
		
		DefaultTableModel modeloempleado= new DefaultTableModel(null,cabecera) {
			@Override
			public boolean isCellEditable(int row, int column) {
			if (column==6) {
				return true;
			} else {
				return false;
			}
		}
		};
		ArrayList<UsuarioAdministrador> administradores= new UsuarioAdministradorDAO().getAll();
		ArrayList<UsuarioEmpleado> empleados= new UsuarioEmpleadoDAO().getAll();
		for (UsuarioAdministrador usuarioAdministrador : administradores) {
			if (!usuarioAdministrador.getTipo_usuario().equals("Empleado")) {
			if (usuarioAdministrador.getEstado_cuenta()==true) {
				Vector<Object> vector= new Vector<Object>();
				vector.add(usuarioAdministrador.getId_usuario());
				vector.add(usuarioAdministrador.getUsuario());
				vector.add(usuarioAdministrador.getNombre_usuario()+" "+usuarioAdministrador.getApellido_usuario());
				vector.add(usuarioAdministrador.getCorreo_usuario());
				vector.add(usuarioAdministrador.getTipo_usuario());
				vector.add("-------------");
				modeloempleado.addRow(vector);
			}
			}
		}
		
		for (UsuarioEmpleado usuarioEmpleado : empleados) {
			if (usuarioEmpleado.getTipo_usuario().equals("Empleado")) {
				if (usuarioEmpleado.getEstado_cuenta()==true) {
				Vector<Object> vector= new Vector<Object>();
				vector.add(usuarioEmpleado.getId_usuario());
				vector.add(usuarioEmpleado.getUsuario());
				vector.add(usuarioEmpleado.getNombre_usuario()+" "+usuarioEmpleado.getApellido_usuario());
				vector.add(usuarioEmpleado.getCorreo_usuario());
				vector.add(usuarioEmpleado.getTipo_usuario());
				vector.add(usuarioEmpleado.getSector_empleado());
				modeloempleado.addRow(vector);
			}
			}
		}
		this.getVistaprincipal().getTable().setModel(modeloempleado);
				//oculta el id de la tabla
				getVistaprincipal().getTable().getColumnModel().getColumn(0).setMaxWidth(0);
				getVistaprincipal().getTable().getColumnModel().getColumn(0).setMinWidth(0);
				getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
				getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getSource().equals(getVista().getBtnEmpleados())) {
			
			this.getVistaprincipal().getBtnInformes().setBorder(null);
			this.getVistaprincipal().getBtnProductos().setBorder(null);
		}
		if (e.getSource().equals(getVistaprincipal().getBtnInformes())) {
			this.getVista().getPanelEmpleados().setVisible(false);
		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		super.focusLost(e);
		if (e.getSource().equals(getVista().getBtnEmpleados())) {
			this.getVista().getBtnEmpleados().setBorder(null);
		}	
	}
	
	public void windowActivated(WindowEvent e) {
		if (this.getVistaprincipal().getBtnProductos().isSelected() || this.getVistaprincipal().getBtnInformes().isSelected()
				|| this.getVistaprincipal().getPanelProducto().isVisible()) {
			
			this.getVista().getPanelEmpleados().setVisible(false);
			super.windowActivated(e);
		}else if (this.getVista().getBtnEmpleados().isSelected() ||this.getVista().getPanelEmpleados().isVisible() ){
			this.actualizarTablaEmpleados();
		}

		}
	
	public void valueChanged(ListSelectionEvent e) {
		super.valueChanged(e);
		int filas_seleccionadas=this.getVistaprincipal().getTable().getSelectedRowCount();
		if (filas_seleccionadas==0) {
			this.habilitacionBotonEmpleado(false);
		}else {
			this.habilitacionBotonEmpleado(true);
		}

	}
	
	public void keyReleased(KeyEvent e) {
		
		if (e.getSource().equals(getVistaprincipal().getTxtBuscarEmpleado())) {
			
			getVistaprincipal().getTable().setModel(new UsuarioAdministradorDAO().buscadorTablaUsuario(getVistaprincipal().getTxtBuscarEmpleado().getText()));
			//oculta el id de la tabla
			getVistaprincipal().getTable().getColumnModel().getColumn(0).setMaxWidth(0);
			getVistaprincipal().getTable().getColumnModel().getColumn(0).setMinWidth(0);
			getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
			getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
			
			
		}
		super.keyReleased(e);
		
	}
		
	public VistaPrincipalAdministrador getVista() {
		return vista;
	}



	public void setVista(VistaPrincipalAdministrador vista) {
		this.vista = vista;
	}


	

}
