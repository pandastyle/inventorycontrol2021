package controlador;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import excepciones.ExcepcionCamposVacios;
import excepciones.ExcepcionConfirmacionPassword;
import excepciones.ExcepcionSector;
import excepciones.ExcepcionSectorExistencia;
import excepciones.Mensajes;
import excepciones.ExcepcionUsuarioExistencia;
import modelo.MD5;
import modelo.Sector;
import modelo.SectorDAO;
import modelo.Usuario;
import modelo.UsuarioAdministrador;
import modelo.UsuarioAdministradorDAO;
import modelo.UsuarioEmpleado;
import modelo.UsuarioEmpleadoDAO;
import vista.VistaNuevoUsuario;

public class ControladorNuevoUsuario implements ActionListener, ItemListener, FocusListener{
	private VistaNuevoUsuario vista;
	private Usuario usu;
	

	public ControladorNuevoUsuario(UsuarioAdministrador usuADM) {
		
		this.setVista(new VistaNuevoUsuario(this) );
		this.getVista().setVisible(true);
		if (usuADM!=null) {
			this.seteosModificar(usuADM);
			usu= usuADM;
		}
		
	}
	
	public ControladorNuevoUsuario(UsuarioEmpleado usuEmp) {
		
		this.setVista(new VistaNuevoUsuario(this) );
		this.getVista().setVisible(true);
		if (usuEmp!=null) {
			this.seteosModificar(usuEmp);
			this.getVista().getBoxSector().setSelectedItem(usuEmp.getSector_empleado());
			usu= usuEmp;
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if (e.getSource().equals(getVista().getBtnRegistrar())) {//REGISTRAR USUARIO
			
			
				if (this.getVista().getTxtNombre().getText().isEmpty() || this.getVista().getTxtApellido().getText().isEmpty() || this.getVista().getTxtUsuario().getText().isEmpty()
						|| new String(this.getVista().getTxtPass().getPassword()).equals("") || new String(this.getVista().getTxtConfirPass().getPassword()).isEmpty() || 
						String.valueOf(this.getVista().getBoxTipoUsuario().getSelectedItem()).equals("Seleccionar")) {
					try {
						throw new ExcepcionCamposVacios();
						}catch(ExcepcionCamposVacios ecv) {
					
					//asteriscos
                     this.getVista().getLblAstNomb().setVisible(true);
                     this.getVista().getLblAstApe().setVisible(true);
                     this.getVista().getLblAstUsuario().setVisible(true);
                     this.getVista().getLblAstPass().setVisible(true);
                     this.getVista().getLblAstConfPass().setVisible(true);
                     this.getVista().getLblAstRol().setVisible(true);
                     this.getVista().getLblAstSector().setVisible(true);
                     JOptionPane.showMessageDialog(this.getVista(), Mensajes.verificacionCamposVacios, "InventoryControl ", 1);
						}
				}else {
					
					try {
			
			Integer usuario= Integer.valueOf(getVista().getTxtUsuario().getText());
			String contra= new String(getVista().getTxtPass().getPassword());	
			String contraconfirm=new String(getVista().getTxtConfirPass().getPassword());
			String tipousuario=(String) this.getVista().getBoxTipoUsuario().getSelectedItem();
			String sector=null;//solo para empleado
			String nombre_usuario= this.getVista().getTxtNombre().getText();
			String apellido_usuario=this.getVista().getTxtApellido().getText();
			String correo=this.getVista().getTxtCorreo().getText();
			String telefono= this.getVista().getTxtTelefono().getText();
			
			if (!contra.equals(contraconfirm)) {
				try {
					throw new ExcepcionConfirmacionPassword();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.confirmacionPassword, "InventoryControl ", 1);
				}
			}else {
				if (String.valueOf(this.getVista().getBoxTipoUsuario().getSelectedItem()).equals("Empleado")) {
					if(String.valueOf(this.getVista().getBoxSector().getSelectedItem()).equals("SELECCIONAR")) {
						throw new ExcepcionSector();
					}
					sector=String.valueOf(this.getVista().getBoxSector().getSelectedItem());		
				}
				
				Integer validacion=null;
				if (sector==null) {
					UsuarioAdministrador administrador= new UsuarioAdministrador(null, usuario, MD5.encriptar(contra), nombre_usuario, apellido_usuario, telefono, correo, tipousuario, true);
				if(this.getVista().getBtnRegistrar().getText().equals("Registrar")) {
					try {
						validacion=new UsuarioAdministradorDAO().insert(administrador);
					} catch (ExcepcionUsuarioExistencia e1) {
						JOptionPane.showMessageDialog(this.getVista(), Mensajes.usuarioExistencia, "InventoryControl", 1);
						validacion=-1;
					}
				}else if(this.getVista().getBtnRegistrar().getText().equals("Modificar")) {
					administrador.setId_usuario(usu.getId_usuario());
					Boolean vali;
					try {
						vali = new UsuarioAdministradorDAO().update(administrador);
					} catch (ExcepcionUsuarioExistencia e1) {
						vali=false;
						JOptionPane.showMessageDialog(this.getVista(), Mensajes.usuarioExistencia, "InventoryControl", 1);
						
					}
					if (vali) {
						validacion=1;
					}else {
						validacion=-1;
					}
				}
					
				}else {
					
					UsuarioEmpleado empleado= new UsuarioEmpleado(null, usuario, MD5.encriptar(contra), nombre_usuario, apellido_usuario, telefono, correo, tipousuario, true, sector);
					
					if(this.getVista().getBtnRegistrar().getText().equals("Registrar")) {
						try {
							validacion=new UsuarioEmpleadoDAO().insert(empleado);
						} catch (ExcepcionUsuarioExistencia e1) {
							JOptionPane.showMessageDialog(this.getVista(), Mensajes.usuarioExistencia, "InventoryControl", 1);
							validacion=-1;
						}
					}else if(this.getVista().getBtnRegistrar().getText().equals("Modificar")) {
						empleado.setId_usuario(usu.getId_usuario());
						Boolean vali;
						try {
							vali = new UsuarioEmpleadoDAO().update(empleado);
						} catch (ExcepcionUsuarioExistencia e1) {
							vali=false;
							JOptionPane.showMessageDialog(this.getVista(), Mensajes.usuarioExistencia, "InventoryControl", 1);
						}
						if (vali) {
							validacion=1;
						}else {
							validacion=-1;
						
						}
					}
				}
				if (validacion!=-1) {
					if(this.getVista().getBtnRegistrar().getText().equals("Registrar")) {
					JOptionPane.showMessageDialog(getVista(), "Usuario Registrado Exitosamente");
					this.getVista().dispose();
					}else if(this.getVista().getBtnRegistrar().getText().equals("Modificar")) {
						JOptionPane.showMessageDialog(getVista(), "Usuario Modificado Exitosamente");
						this.getVista().dispose();
					}
				}else {
					if(this.getVista().getBtnRegistrar().getText().equals("Registrar")) {
						JOptionPane.showMessageDialog(getVista(), "No se pudo registrar el usuario, int�ntelo nuevamente");
					}else if(this.getVista().getBtnRegistrar().getText().equals("Modificar")) {
						JOptionPane.showMessageDialog(getVista(), "No se pudo modificar el usuario, int�ntelo nuevamente");
						
					}
					
				}
				
			}
	
		}catch(NumberFormatException num){
			JOptionPane.showMessageDialog(this.getVista(), Mensajes.tipo_datos, "InventoryControl", 1);
		}catch(ExcepcionSector es){
			JOptionPane.showMessageDialog(this.getVista(), Mensajes.sectorValido, "InventoryControl", 1);
		}
		}
		}
		
		
		if (e.getSource().equals(getVista().getBtnCancelar())) {//CANCELAR
			this.getVista().dispose();
		}
		
		////////////////BOTONES DE SECTOR
		if (e.getSource().equals(getVista().getBtnNuevoSector())) {//NUEVO SECTOR
			Sector sec= new Sector(null, JOptionPane.showInputDialog(vista, "Registrar Nuevo Sector", "InventoryControl- Nuevo Sector", 1), true);
			if (sec.getNombre_sector()!=null) {
			try {
				if (new SectorDAO().insert(sec)!=-1) {
					this.getVista().getBoxSector().removeAllItems();
					this.getVista().cargarSector();
					JOptionPane.showMessageDialog(getVista(), "Sector Registrado Exitosamente");
				}
			}catch (ExcepcionSectorExistencia e1) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.sectorExistencia, "InventoryControl", 1);
			}
			}
		}
		try {
		if (e.getSource().equals(getVista().getBtnModificarSec())) {//MODIFICAR SECTOR
			ArrayList<Sector> sectores = new SectorDAO().getAll();
			Sector sec = null;
			if (String.valueOf(this.getVista().getBoxSector().getSelectedItem()).equals("SELECCIONAR")&&String.valueOf(this.getVista().getBoxTipoUsuario().getSelectedItem()).equals("Empleado")) {
					throw new ExcepcionSector();
			}	
			for (Sector sector : sectores) {
				if (sector.getNombre_sector().equals(String.valueOf(this.getVista().getBoxSector().getSelectedItem()))) {
					sec= sector;
				}
			}
			sec.setNombre_sector(JOptionPane.showInputDialog(this.getVista(), "Modificar Sector", sec.getNombre_sector()));
			if (sec.getNombre_sector()!=null) {
			try {
				if (new SectorDAO().update(sec)) {
					this.getVista().getBoxSector().removeAllItems();
					this.getVista().cargarSector();
					JOptionPane.showMessageDialog(getVista(), "Sector Modificado Exitosamente");
				}
			}catch (ExcepcionSectorExistencia e1) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.sectorExistencia, "InventoryControl", 1);
			}
			}
			
		}
		if (e.getSource().equals(getVista().getBtnEliminarSec())) {//ELIMINAR SECTOR
			ArrayList<Sector> sectores = new SectorDAO().getAll();
			Sector sec = null;
			if (String.valueOf(this.getVista().getBoxSector().getSelectedItem()).equals("SELECCIONAR")&&String.valueOf(this.getVista().getBoxTipoUsuario().getSelectedItem()).equals("Empleado")) {
					throw new ExcepcionSector();
			}	
			for (Sector sector : sectores) {
				if (sector.getNombre_sector().equals(String.valueOf(this.getVista().getBoxSector().getSelectedItem()))) {
					sec= sector;
				}
			}
			Integer confirmacion= JOptionPane.showConfirmDialog(getVista(), "�Seguro que desea eliminar '"+sec.getNombre_sector()+"'?");
			if (confirmacion==0) {
				if (new SectorDAO().baja(sec)) {
					this.getVista().getBoxSector().removeAllItems();
					this.getVista().cargarSector();
					JOptionPane.showMessageDialog(getVista(), "Sector Eliminado Exitosamente");
				}
			}
			
		}
		}catch(ExcepcionSector es) {
			JOptionPane.showMessageDialog(this.getVista(), Mensajes.sectorValido, "InventoryControl", 1);
		}
	}

	

	@Override
	public void itemStateChanged(ItemEvent e) {
		
		if (!String.valueOf(this.getVista().getBoxTipoUsuario().getSelectedItem()).equals("Empleado")) {
			this.getVista().getBoxSector().setEnabled(false);
			this.getVista().getBtnNuevoSector().setEnabled(false);
			this.getVista().getBtnModificarSec().setEnabled(false);
			this.getVista().getBtnEliminarSec().setEnabled(false);
			
		}else {
			this.getVista().getBoxSector().setEnabled(true);
			this.getVista().getBtnNuevoSector().setEnabled(true);
			this.getVista().getBtnModificarSec().setEnabled(true);
			this.getVista().getBtnEliminarSec().setEnabled(true);
		}
		
	}
	
	public void seteosModificar(Usuario usu) {
		this.getVista().setTitle("InventoryControl - Modificar Usuario");
		this.getVista().getBtnRegistrar().setText("Modificar");
		this.getVista().getTxtUsuario().setText(String.valueOf(usu.getUsuario()));
		this.getVista().getTxtNombre().setText(usu.getNombre_usuario());
		this.getVista().getTxtApellido().setText(usu.getApellido_usuario());
		this.getVista().getBoxTipoUsuario().setSelectedItem(usu.getTipo_usuario());
		if (usu.getCorreo_usuario()!=null) {
			this.getVista().getTxtCorreo().setText(usu.getCorreo_usuario());
		}else {
			this.getVista().getTxtCorreo().setText("");
		}
		if (usu.getContacto_usuario()!=null) {
			this.getVista().getTxtTelefono().setText(usu.getContacto_usuario());
		}else {
			this.getVista().getTxtTelefono().setText("");
		}
		
	}

	public VistaNuevoUsuario getVista() {
		return vista;
	}

	public void setVista(VistaNuevoUsuario vista) {
		this.vista = vista;
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}
 
	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource().equals(this.getVista().getTxtNombre())) {
			if (this.getVista().getTxtNombre().getText().equals("")) {
				this.getVista().getTxtNombre().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstNomb().setVisible(true);
			}else {
				this.getVista().getTxtNombre().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstNomb().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtApellido())) {
			if (this.getVista().getTxtApellido().getText().equals("")) {
				this.getVista().getTxtApellido().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstApe().setVisible(true);
			}else {
				this.getVista().getTxtApellido().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstApe().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtUsuario())) {
			if (this.getVista().getTxtUsuario().getText().equals("")) {
				this.getVista().getTxtUsuario().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstUsuario().setVisible(true);
			}else {
				this.getVista().getTxtUsuario().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstUsuario().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtPass())) {
			if (new String(this.getVista().getTxtPass().getPassword()).equals("")) {
				this.getVista().getTxtPass().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstPass().setVisible(true);
			}else {
				this.getVista().getTxtPass().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstPass().setVisible(false);
			}
			}
		
		if (e.getSource().equals(this.getVista().getTxtConfirPass())) {
			if (new String(this.getVista().getTxtConfirPass().getPassword()).equals("")) {
				this.getVista().getTxtConfirPass().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstConfPass().setVisible(true);
		}	else {
			this.getVista().getTxtConfirPass().setBorder(new LineBorder(Color.lightGray));
			this.getVista().getLblAstConfPass().setVisible(false);
		}
	    }
		
		if (e.getSource().equals(this.getVista().getBoxTipoUsuario())) {
			if (this.getVista().getBoxTipoUsuario().getSelectedIndex()==0) {
				this.getVista().getBoxTipoUsuario().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstRol().setVisible(true);
		}	else {
			this.getVista().getBoxTipoUsuario().setBorder(new LineBorder(Color.lightGray));
			this.getVista().getLblAstRol().setVisible(false);
		}
	    }
		
		if (e.getSource().equals(this.getVista().getBoxSector())) {
			if (this.getVista().getBoxSector().getSelectedIndex()==-1) {
				this.getVista().getBoxSector().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstSector().setVisible(true);
		}	else {
			this.getVista().getBoxSector().setBorder(new LineBorder(Color.lightGray));
			this.getVista().getLblAstSector().setVisible(false);
		}
	    }
	
	}	

}
