package controlador;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
//NOTA* QUEDA POR FALTAR EL GENERAR EL NOMBRE DE LA IMAGEN QUE DEBE SER = al ID DEL PRODUCTO 
//SE DEBE OBTENER EL ID DEL ULTIMO PRODUCTO Y SUMARLE 1
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.StyledEditorKit.ForegroundAction;

import excepciones.ExcepcionCamposVacios;
import excepciones.ExcepcionCategoriaExistencia;
import excepciones.ExcepcionCodigoExistencia;
import excepciones.ExcepcionFecha;
import excepciones.ExcepcionFechaCaducidad;
import excepciones.ExcepcionMarcaExistencia;
import excepciones.ExcepcionNegativoCero;
import excepciones.Mensajes;
import modelo.BasedeDatos;
import modelo.Categoria;
import modelo.CategoriaDAO;
import modelo.Marca;
import modelo.MarcaDAO;
import modelo.Producto;
import modelo.ProductoDAO;
import modelo.Utils;
import vista.VistaNuevoProducto;

public class ControladorVistaNuevoProducto implements ActionListener, ChangeListener, FocusListener {
	
	private VistaNuevoProducto vista;
	private JFileChooser fc = new JFileChooser();
	private ProductoDAO productoDao = new ProductoDAO();
	private ImageIcon imagenpreview;
	private String origen="";
	private Integer idprodmodif;
	 
	public ControladorVistaNuevoProducto() {
		this.setVista(new VistaNuevoProducto(this));
		this.getVista().setVisible(true);
	}
	
	public ControladorVistaNuevoProducto(Integer id) {
		this.setVista(new VistaNuevoProducto(this));
		this.getVista().setVisible(true);
		this.setIdprodmodif(id);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(getVista().getBtnCancelar())) {  //cierra ventana con boton cancelar
		this.getVista().dispose();
		}
		
		if (e.getSource().equals(getVista().getBtnImagen())) {  //abre el file chooser y al elegir el archivo muestra la url en el cuadro de texto
			int ap = fc.showOpenDialog(getVista());		
			
			if (ap == JFileChooser.APPROVE_OPTION) {
				origen = fc.getSelectedFile().getAbsolutePath();
			
				 imagenpreview = new ImageIcon(new ImageIcon(origen).getImage().getScaledInstance
		    			 (this.getVista().getLblverFoto().getWidth(),this.getVista().getLblverFoto().getHeight(),Image.SCALE_DEFAULT));
				this.getVista().getLblverFoto().setIcon(imagenpreview);
			}
		}
		
		if (e.getSource().equals(getVista().getBtnRegistrar())) {   // REGISTRAR
			if(this.getVista().getTxtNombre().equals("") || this.getVista().getTxtCodigo().equals("") ||
					this.getVista().getTxtStock().equals("") || this.getVista().getTxtPrecio().equals("")
					||getVista().getTextAreaDescripcion().getText().equals("") 
					|| (getVista().getRdbtnSi().isSelected() && this.getVista().getTxtFechaCad().getText().equals("")) 
					|| (!getVista().getRdbtnSi().isSelected() && !getVista().getRdbtnNo().isSelected())) {
				try {
				throw new ExcepcionCamposVacios();
				}catch(ExcepcionCamposVacios ecv) {
					
					//astericos
					this.getVista().getLblAstCat().setVisible(true);
					this.getVista().getLblAstCod().setVisible(true);
					this.getVista().getLblAstDescr().setVisible(true);
					this.getVista().getLblAstMarc().setVisible(true);
					this.getVista().getLblAstNomb().setVisible(true);
					this.getVista().getLblAstPrec().setVisible(true);
					this.getVista().getLblAstStock().setVisible(true);
					this.getVista().getLblAstSeleccion().setVisible(true);
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.verificacionCamposVacios, "InventoryControl", 1);
				}
			}else {
				
				Date fecha_actual=new Date(System.currentTimeMillis());
			try {
			LocalDate fecha=null;
			Date fecha_caducidad=null;
			String destinofoto="assets/img"+productoDao.numImagen()+".png";

			if(getVista().getRdbtnSi().isSelected()){// si el articulo tiene caducidad se toma el label 
				Integer dia,mes,anio;
				String[] datosFecha= this.getVista().getTxtFechaCad().getText().split("/");
				dia=Integer.valueOf(datosFecha[0]);
				mes=Integer.valueOf(datosFecha[1]);
				anio=Integer.valueOf(datosFecha[2]);
				if((dia<=0 || dia>31)||(mes<=0 || mes>12)||anio<=2000) {
					throw new ExcepcionFecha();
				}
				fecha= LocalDate.of(anio, mes, dia);
				fecha_caducidad= Date.valueOf(fecha);
				if (fecha_caducidad.before(fecha_actual)) {
					throw new ExcepcionFechaCaducidad();
				}
			}

			
			
			Integer cod_producto=Integer.parseInt(getVista().getTxtCodigo().getText());
			String nombre_prod=getVista().getTxtNombre().getText();
			String descripcion=getVista().getTextAreaDescripcion().getText();
			String nombre_marca=String.valueOf(getVista().getcBoxMarca().getSelectedItem());
			String nombre_categoria= String.valueOf(getVista().getcBoxCategoria().getSelectedItem());
			Integer stock=Integer.parseInt(getVista().getTxtStock().getText());
			Double precio=Double.parseDouble(getVista().getTxtPrecio().getText());
			String urlfoto= destinofoto;
			
			if((cod_producto <=0) ||(stock <=0 ||(precio <=0))) {
				throw new ExcepcionNegativoCero();
			}
			Producto prod ;
			if(fecha_caducidad!=null) {
				prod= new Producto(null, cod_producto, nombre_prod, descripcion, nombre_marca,
						nombre_categoria, fecha_caducidad, stock, precio, true, urlfoto);
			}else {
				prod= new Producto(null, cod_producto, nombre_prod, descripcion, nombre_marca, nombre_categoria, stock, precio, true, urlfoto);
			}
			
			
			if (this.getVista().getBtnRegistrar().getText().equals("Registrar")) {//si estamos registrando
				try {
					if (this.getProductoDao().insert(prod) != -1) {
						try {
							Utils.copyFile(origen, destinofoto );
							 JOptionPane.showMessageDialog(null, "Producto registrado exitosamente");
							 this.getVista().dispose();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 
					}
				}catch (ExcepcionCodigoExistencia e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.codigoExistencia, "InventoryControl", 1);
				}
			}
			if (this.getVista().getBtnRegistrar().getText().equals("Modificar")) {
				prod.setId_producto(this.getIdprodmodif());
				prod.setFoto("assets/img"+prod.getId_producto()+".png");
				try {
					if (this.getProductoDao().update(prod)) {
						if (origen.equals("")) {
							origen= prod.getFoto();
						}
						try {
							Utils.copyFile(origen, prod.getFoto());
							JOptionPane.showMessageDialog(null, "Producto modificado exitosamente");
							 this.getVista().dispose();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} 
					}
				}  catch (ExcepcionCodigoExistencia e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.codigoExistencia, "InventoryControl", 1);
				}
			} 
				
			}catch(NumberFormatException num) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.tipo_datos, "InventoryControl", 1);
			}catch(ExcepcionNegativoCero z) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.negativoCero, "InventoryControl", 1);
			}
			catch(ExcepcionFechaCaducidad fc) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.fecha_Caducidad, "InventoryControl", 1);
			}
			catch(ExcepcionFecha f) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.fecha, "InventoryControl", 1);
			}catch(ArrayIndexOutOfBoundsException ai) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.fecha, "InventoryControl", 1);
			}
			}	
       }	
		
		//BOTONES DE MARCA
		if(e.getSource().equals(this.getVista().getBtnNuevaMarca())) {//NUEVA MARCA
			Marca mar= new Marca(null,JOptionPane.showInputDialog(this.getVista(), "Registrar Nueva Marca", "InventoryControl- Nueva Marca", 1),true);
			if (mar.getNombre_marca()!=null) {
				try {
					if (new MarcaDAO().insert(mar)!=-1) {
						this.getVista().getcBoxMarca().removeAllItems();
						this.getVista().cargarMarcas();
						JOptionPane.showMessageDialog(getVista(), "Marca Registrada Exitosamente");
					}
				} catch (ExcepcionMarcaExistencia e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.marcaExistencia, "InventoryControl", 1);
				}
			}
			
		}
		
		ArrayList<Marca> marcas= new MarcaDAO().getAll();
		Marca mar=null;
		for (Marca marca : marcas) {
			if (marca.getNombre_marca().equals(String.valueOf(this.getVista().getcBoxMarca().getSelectedItem()))) {
				mar= marca;
			}
		}
		
		if (e.getSource().equals(this.getVista().getBtnModificarMarca())) {//MODIFICAR MARCA
			mar.setNombre_marca(JOptionPane.showInputDialog(this.getVista(), "Modificar Marca", mar.getNombre_marca()));
			if (mar.getNombre_marca()!=null) {
				try {
					if (new MarcaDAO().update(mar)) {
						this.getVista().getcBoxMarca().removeAllItems();
						this.getVista().cargarMarcas();
						JOptionPane.showMessageDialog(getVista(), "Marca Modificada Exitosamente");
					}
				}catch (ExcepcionMarcaExistencia e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.marcaExistencia, "InventoryControl", 1);
				}
			}
		
		}
		
		if (e.getSource().equals(this.getVista().getBtnBorrarMarca())) {// BORRAR MARCA
			Integer confirmacion= JOptionPane.showConfirmDialog(getVista(), "�Seguro que desea eliminar '"+mar.getNombre_marca()+"'?");
			if (confirmacion==0) {
				new MarcaDAO().baja(mar);
				this.getVista().getcBoxMarca().removeAllItems();
				this.getVista().cargarMarcas();
				JOptionPane.showMessageDialog(getVista(), "Marca Eliminada Exitosamente");
			}
			
		}
		
		/////////////BOTONES DE CATEGORIA
		if (e.getSource().equals(this.getVista().getBtnNuevaCategoria())) {//NUEVA CATEGORIA
			Categoria cate= new Categoria(null,JOptionPane.showInputDialog(this.getVista(), "Registrar Nueva Categoria", "InventoryControl- Nueva Categoria", 1),true);
			if (cate.getNombre_categoria()!=null) {
				try {
					if (new CategoriaDAO().insert(cate)!=-1) {
						this.getVista().getcBoxCategoria().removeAllItems();
						this.getVista().cargarCategorias();
						JOptionPane.showMessageDialog(getVista(), "Categoria Registrada Exitosamente");
					}
				}  catch (ExcepcionCategoriaExistencia e1) {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.categoriaExistencia, "InventoryControl", 1);
				}
			}
			
		}
		
		ArrayList<Categoria> categorias= new CategoriaDAO().getAll();
		Categoria cat= null;
		for (Categoria categoria : categorias) {
			if (categoria.getNombre_categoria().equals(this.getVista().getcBoxCategoria().getSelectedItem())) {
				cat= categoria;
			}
		}
		
		if (e.getSource().equals(this.getVista().getBtnModificarCategoria())) {//MODIFICAR CATEGORIA
			cat.setNombre_categoria(JOptionPane.showInputDialog(this.getVista(), "Modificar Categoria", cat.getNombre_categoria()));
			if (cat.getNombre_categoria()!=null) {
			try {
				if (new CategoriaDAO().update(cat)) {
					this.getVista().getcBoxCategoria().removeAllItems();
					this.getVista().cargarCategorias();
					JOptionPane.showMessageDialog(getVista(), "Categoria Modificada Exitosamente");
				}
			}  catch (ExcepcionCategoriaExistencia e1) {
				JOptionPane.showMessageDialog(this.getVista(), Mensajes.categoriaExistencia, "InventoryControl", 1);
			}
			}
		}
		
		if (e.getSource().equals(this.getVista().getBtnBorrarCategoria())) {//ELIMINAR CATEGORIA
			Integer confirmacion= JOptionPane.showConfirmDialog(getVista(), "�Seguro que desea eliminar '"+cat.getNombre_categoria()+"'?");
			if (confirmacion==0) {
				new CategoriaDAO().baja(cat);
				this.getVista().getcBoxCategoria().removeAllItems();
				this.getVista().cargarCategorias();
				JOptionPane.showMessageDialog(getVista(), "Categoria Eliminada Exitosamente");
			}
		}
		
	}
	
	
	
	public ProductoDAO getProductoDao() {
		return productoDao;
	}

	@Override
	public void stateChanged(ChangeEvent e) {

		if(getVista().getRdbtnNo().isSelected()){
			this.getVista().getTxtFechaCad().setEnabled(false);
		}
		if(getVista().getRdbtnSi().isSelected()){
			this.getVista().getTxtFechaCad().setEnabled(true);
		}
	}

	public VistaNuevoProducto getVista() {
		return vista;
	}

	public void setVista(VistaNuevoProducto vista) {
		this.vista = vista;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		
		if (e.getSource().equals(this.getVista().getTxtNombre())) {
			if (this.getVista().getTxtNombre().getText().equals("")) {
				this.getVista().getTxtNombre().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstNomb().setVisible(true);
			}else {
				this.getVista().getTxtNombre().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstNomb().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtCodigo())) {
			if (this.getVista().getTxtCodigo().getText().equals("")) {
				this.getVista().getTxtCodigo().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstCod().setVisible(true);
			}else {
				this.getVista().getTxtCodigo().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstCod().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtStock())) {
			if (this.getVista().getTxtStock().getText().equals("")) {
				this.getVista().getTxtStock().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstStock().setVisible(true);
			}else {
				this.getVista().getTxtStock().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstStock().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTxtPrecio())) {
			if (this.getVista().getTxtPrecio().getText().equals("")) {
				this.getVista().getTxtPrecio().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstPrec().setVisible(true);
			}else {
				this.getVista().getTxtPrecio().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstPrec().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getTextAreaDescripcion())) {
			if (this.getVista().getTextAreaDescripcion().getText().equals("")) {
				this.getVista().getTextAreaDescripcion().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstDescr().setVisible(true);
			}else {
				this.getVista().getTextAreaDescripcion().setBorder(new LineBorder(Color.lightGray));
				this.getVista().getLblAstDescr().setVisible(false);
			}
		}
		
		if (e.getSource().equals(this.getVista().getcBoxCategoria())) {
			if (this.getVista().getcBoxCategoria().getSelectedIndex()==-1) {
				this.getVista().getcBoxCategoria().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstCat().setVisible(true);
		}	else {
			this.getVista().getcBoxCategoria().setBorder(new LineBorder(Color.lightGray));
			this.getVista().getLblAstCat().setVisible(false);
		}
	    }
		
		if (e.getSource().equals(this.getVista().getcBoxMarca())) {
			if (this.getVista().getcBoxMarca().getSelectedIndex()==-1) {
				this.getVista().getcBoxMarca().setBorder(new LineBorder(Color.red));
				this.getVista().getLblAstMarc().setVisible(true);
		}	else {
			this.getVista().getcBoxMarca().setBorder(new LineBorder(Color.lightGray));
			this.getVista().getLblAstMarc().setVisible(false);
		}
	    }
		
	}

	public Integer getIdprodmodif() {
		return idprodmodif;
	}

	public void setIdprodmodif(Integer idprodmodif) {
		this.idprodmodif = idprodmodif;
	}
	


}
