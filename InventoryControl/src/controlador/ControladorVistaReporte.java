package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;



import excepciones.Mensajes;
import modelo.BasedeDatos;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaReporte;

public class ControladorVistaReporte implements ActionListener {
	private VistaReporte vista;
	
	

	public ControladorVistaReporte() {
		this.setVista(new VistaReporte(this));
		this.getVista().setVisible(true);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnCancelar())) {
			this.getVista().dispose();
		}
		
		if (e.getSource().equals(getVista().getBtnAceptar())) {
			Date flagfecha=new Date(0);
			Date fecha_actual=new Date(System.currentTimeMillis());
			try {
			Integer dia,mes,anio;
			String[] datosFecha= this.getVista().getTxtFechaInicio().getText().split("/");
			dia=Integer.valueOf(datosFecha[0]);
			mes=Integer.valueOf(datosFecha[1]);
			anio=Integer.valueOf(datosFecha[2]);
			LocalDate fecha= LocalDate.of(anio, mes, dia);
			Date fecha_inicio= Date.valueOf(fecha);
			if(fecha_inicio.after(fecha_actual)) {
				flagfecha=fecha_inicio;
				throw new Exception();
			}
			
			String[] datosFecha2= this.getVista().getTxtFechaFin().getText().split("/");
			dia=Integer.valueOf(datosFecha2[0]);
			mes=Integer.valueOf(datosFecha2[1]);
			anio=Integer.valueOf(datosFecha2[2]);
			fecha= LocalDate.of(anio, mes, dia);
			Date fecha_fin= Date.valueOf(fecha);
			if(fecha_fin.after(fecha_actual)) {
				flagfecha=fecha_fin;
				throw new Exception();
			}
			
			try {
				this.getVista().dispose();
				Map parameters= new HashMap();
				parameters.put("fecha_inicio", fecha_inicio);
				parameters.put("fecha_fin", fecha_fin);
				Connection con= BasedeDatos.getInstance().getConexion();
				JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("reportes/reportprodcaducidad.jasper");
				JasperPrint jp= JasperFillManager.fillReport(report, parameters, con);
				JasperViewer.viewReport(jp,false);
			} catch (JRException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			}catch(Exception e3) {
				if(flagfecha.after(fecha_actual)) {
					this.getVista().getLblAdvertencia().setVisible(true);
				}else {
					JOptionPane.showMessageDialog(this.getVista(), Mensajes.tipo_datos, "InventoryControl ", 1);
				}
			
			}
		
			
		}
		
	}



	public VistaReporte getVista() {
		return vista;
	}



	public void setVista(VistaReporte vista) {
		this.vista = vista;
	}

	
}
