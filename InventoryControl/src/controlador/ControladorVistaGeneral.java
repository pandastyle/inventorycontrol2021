package controlador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;



import controlador.ControladorVistaGeneral;
import modelo.BasedeDatos;
import modelo.Producto;
import modelo.ProductoDAO;
import modelo.Usuario;
import modelo.UsuarioAdministrador;
import modelo.UsuarioAdministradorDAO;
import modelo.UsuarioEmpleado;
import modelo.UsuarioEmpleadoDAO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaPrincipalGeneral;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public abstract class ControladorVistaGeneral implements ActionListener, KeyListener, WindowListener, ListSelectionListener, FocusListener {
	private VistaPrincipalGeneral vistaprincipal;
	private UsuarioAdministrador usuAdmin=null;
	private UsuarioEmpleado usuEmp=null;
	
	
	public ControladorVistaGeneral(Usuario usu) {
		if (usu.getTipo_usuario().equals("Empleado")) {
			 usuEmp= new UsuarioEmpleadoDAO().get(usu.getId_usuario());
		}else {
			usuAdmin= new UsuarioAdministradorDAO().get(usu.getId_usuario());
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		///////////PRODUCTOS////////////////////////////////
	if(e.getSource().equals(getVistaprincipal().getBtnProductos())) {//boton productos del panel principal
		this.getVistaprincipal().getTxtBuscar().setVisible(true);
		this.getVistaprincipal().getTxtBuscarEmpleado().setVisible(false);
		this.actualizarModeloTabla();
		this.getVistaprincipal().getPanelProducto().setVisible(true);//hace visible el Panel
		this.getVistaprincipal().getBtnProductos().setBorder(BorderFactory.createLineBorder(Color.GREEN));
		this.getVistaprincipal().getPanelInformes().setVisible(false);
		this.getVistaprincipal().getBtnInformes().setBorder(null);
		this.habilitacionBotones(false);
	}
	//---------------NUEVO PRODUCTO----------------------
	if(e.getSource().equals(getVistaprincipal().getBtnNuevoProducto())) {
		
		new ControladorVistaNuevoProducto();
	}
	//////////////////////////
	
	//---------------MODIFICAR PRODUCTO----------------------
	if (e.getSource().equals(getVistaprincipal().getBtnModificarProducto())) {
		Integer idprod=	(Integer) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 0);
		
		Producto product=new ProductoDAO().get(idprod);
		ControladorVistaNuevoProducto modificar= new ControladorVistaNuevoProducto(product.getId_producto());
		modificar.getVista().setTitle("InventoryControl - ModificarProducto");
		modificar.getVista().getBtnRegistrar().setText("Modificar");
		//falta setear los componentes
		modificar.getVista().getTxtNombre().setText(product.getNombre_producto());
		modificar.getVista().getcBoxMarca().setSelectedItem(product.getNombre_marca());
		modificar.getVista().getcBoxCategoria().setSelectedItem(product.getNombre_categoria());
	
		if(product.getFecha_caducidad()!=null) {
			
		String fecha=String.valueOf(product.getFecha_caducidad());
		Integer dia,mes,anio;
		String[] datosFecha= fecha.split("-");
		dia=Integer.valueOf(datosFecha[2]);
		mes=Integer.valueOf(datosFecha[1]);
		anio=Integer.valueOf(datosFecha[0]);
		String fechatxt=dia+"/"+mes+"/"+anio;
		
		if (product.getFecha_caducidad()!=null) {
			modificar.getVista().getRdbtnSi().setSelected(true);
			modificar.getVista().getTxtFechaCad().setText(fechatxt);
		
		}}
		if (product.getFecha_caducidad()==null) {
			modificar.getVista().getRdbtnNo().setSelected(true);
			modificar.getVista().getTxtFechaCad().setText("no posee");
		}

		modificar.getVista().getLblverFoto().setIcon(new ImageIcon(new ImageIcon(product.getFoto()).getImage().getScaledInstance
   			 (modificar.getVista().getLblverFoto().getWidth(),modificar.getVista().getLblverFoto().getHeight(),Image.SCALE_DEFAULT)));
		modificar.getVista().getTxtCodigo().setText(String.valueOf(product.getCod_producto()));
		modificar.getVista().getTxtStock().setText(String.valueOf(product.getStock()));
		modificar.getVista().getTxtPrecio().setText(String.valueOf(product.getPrecio()));
		modificar.getVista().getTextAreaDescripcion().append(product.getDescripcion());
		modificar.getVista().getTextAreaDescripcion().setCaretPosition(modificar.getVista().getTextAreaDescripcion().getDocument().getLength());
		
		
	}
	//---------------ELIMINAR PRODUCTO----------------------
	if (e.getSource().equals(getVistaprincipal().getBtnEliminarProducto())) {
		Integer idprod=	(Integer) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 0);
    	Producto product=new ProductoDAO().get(idprod);
		Integer confirmacion=JOptionPane.showConfirmDialog(vistaprincipal,"�Seguro que quiere eliminar "+product.getNombre_producto()+"?", "Eliminar Producto", 1, 2);
		if (confirmacion==0) {
			if (new ProductoDAO().baja(product)) {
				JOptionPane.showMessageDialog(getVistaprincipal(), "Producto Eliminado Exitosamente");
				
			}
		}
		
	}
	//---------------DETALLE PRODUCTO----------------------
	if (e.getSource().equals(getVistaprincipal().getBtnDetalleProducto())) {
		ControladorVistaDetalleProducto detalleproducto=new ControladorVistaDetalleProducto();
		Integer idprod=	(Integer) this.getVistaprincipal().getTable().getValueAt(this.getVistaprincipal().getTable().getSelectedRow(), 0);
    	Producto product=new ProductoDAO().get(idprod);
    	detalleproducto.getVista().getLblNombreProducto().setText(product.getNombre_producto());
    	detalleproducto.getVista().getLblMarca().setText("Marca: "+product.getNombre_marca());
    	detalleproducto.getVista().getLblCategoria().setText("Categor�a: "+product.getNombre_categoria());
    	detalleproducto.getVista().getLblStock().setText("Stock: "+String.valueOf(product.getStock()));
    	if(product.getFecha_caducidad()!=null) {
    	String fecha=String.valueOf(product.getFecha_caducidad());
		Integer dia,mes,anio;
		String[] datosFecha= fecha.split("-");
		dia=Integer.valueOf(datosFecha[2]);
		mes=Integer.valueOf(datosFecha[1]);
		anio=Integer.valueOf(datosFecha[0]);
		String fechatxt=dia+"/"+mes+"/"+anio;
		if (product.getFecha_caducidad()!=null) {

			detalleproducto.getVista().getLblfechaCaducidad().setText("Caducidad: "+fechatxt);
		}
    	}
		detalleproducto.getVista().getLblPrecio().setText("Precio: $"+String.valueOf(product.getPrecio()));
		detalleproducto.getVista().getTextArea().append(product.getDescripcion());
		detalleproducto.getVista().getLblFoto().setIcon(new ImageIcon(new ImageIcon(product.getFoto()).getImage().getScaledInstance
	   			 (detalleproducto.getVista().getLblFoto().getWidth(),detalleproducto.getVista().getLblFoto().getHeight(),Image.SCALE_DEFAULT)));
			
	}
	///////////////////INFORMES////////////////////////
	if (e.getSource().equals(getVistaprincipal().getBtnInformes())) {
		this.actualizarModeloTabla();
		this.getVistaprincipal().getBtnInformes().setBorder(BorderFactory.createLineBorder(Color.GREEN));
		this.getVistaprincipal().getBtnProductos().setBorder(null);
		this.getVistaprincipal().getPanelProducto().setVisible(false);
		this.getVistaprincipal().getPanelInformes().setVisible(true);
		this.getVistaprincipal().getTxtBuscar().setVisible(true);
		this.getVistaprincipal().getTxtBuscarEmpleado().setVisible(false);
	}
	if (e.getSource().equals(this.getVistaprincipal().getBtnInformeGrafico())) {
		try {
			Connection con= BasedeDatos.getInstance().getConexion();
			JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("reportes/reportgrafico.jasper");
			JasperPrint jp= JasperFillManager.fillReport(report, null, con);
			JasperViewer.viewReport(jp,false);
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	if (e.getSource().equals(getVistaprincipal().getBtnInformeGanancias())) {
		try {
			Map parameters= new HashMap();
			Date fecha= new Date(System.currentTimeMillis());
			parameters.put("fecha_actual", fecha);
			Connection con= BasedeDatos.getInstance().getConexion();
			JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("reportes/reportgananestimadas.jasper");
			JasperPrint jp= JasperFillManager.fillReport(report, parameters, con);
			JasperViewer.viewReport(jp,false);
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	if (e.getSource().equals(getVistaprincipal().getBtnInformesCaducado())) {
		new ControladorVistaReporte();
	}
	
	
	////////////////////MENU//////////////////////////////
	if (e.getSource().equals(getVistaprincipal().getBtnCerrarSesion())) {
		Integer elige = JOptionPane.showConfirmDialog(vistaprincipal,"�Seguro que quiere cerrar sesi�n?", "InventoryControl- Cerrar Sesi�n", 1, 2);
		if (elige == 0) {
			this.getVistaprincipal().dispose();
			new ControladorVistaLogin();
		}	
		}
	
	if (e.getSource().equals(getVistaprincipal().getBtncuenta())) {
		if (usuAdmin==null) {
			ControladorNuevoUsuario control =new ControladorNuevoUsuario(usuEmp);
			this.modificacionMiCuenta(control);
		}else {
			ControladorNuevoUsuario control2 =new ControladorNuevoUsuario(usuAdmin);
			this.modificacionMiCuenta(control2);
		}
		
	}
	}
	
	public void modificacionMiCuenta(ControladorNuevoUsuario control) {
		control.getVista().setTitle("InventoryControl- Mi Cuenta");
		control.getVista().getBoxSector().setEnabled(false);
		control.getVista().getBoxTipoUsuario().setEnabled(false);
		control.getVista().getTxtUsuario().setEnabled(false);
		control.getVista().getBtnModificarSec().setEnabled(false);
		control.getVista().getBtnNuevoSector().setEnabled(false);
		control.getVista().getBtnEliminarSec().setEnabled(false);
		control.getVista().getTxtNombre().setEnabled(false);
		control.getVista().getTxtApellido().setEnabled(false);
		
	}
	
	protected void actualizarModeloTabla() {
		String cabecera[]= {"id","C�digo","Producto","Categor�a","Marca","Stock"};
		DefaultTableModel modelo= new DefaultTableModel(null,cabecera) {
			@Override
			public boolean isCellEditable(int row, int column) {
			if (column==6) {
				return true;
			} else {
				return false;
			}
		}
		};
		
		ProductoDAO pr = new ProductoDAO();
		
		for ( Producto producto : pr.getAll()) {
			if (producto.getEstado_producto()==true) {
				Vector<Object> vector = new Vector<Object>();
				vector.add(producto.getId_producto());
				vector.add(producto.getCod_producto());
				vector.add(producto.getNombre_producto());
				vector.add(producto.getNombre_categoria());
				vector.add(producto.getNombre_marca());
				vector.add(producto.getStock());
				modelo.addRow(vector);
			}
			
		}
		getVistaprincipal().getTable().setModel(modelo);
		//oculta el id de la tabla
		getVistaprincipal().getTable().getColumnModel().getColumn(0).setMaxWidth(0);
		getVistaprincipal().getTable().getColumnModel().getColumn(0).setMinWidth(0);
		getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
		getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

	}
	
	
	public void habilitacionBotones(Boolean estado) {
		this.getVistaprincipal().getBtnModificarProducto().setEnabled(estado);
		this.getVistaprincipal().getBtnEliminarProducto().setEnabled(estado);
		this.getVistaprincipal().getBtnDetalleProducto().setEnabled(estado);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		int filas_seleccionadas=this.getVistaprincipal().getTable().getSelectedRowCount();
		if (filas_seleccionadas==0) {
			this.habilitacionBotones(false);
		}else {
			this.habilitacionBotones(true);
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
	
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getSource().equals(getVistaprincipal().getTxtBuscar())) {
			getVistaprincipal().getTable().setModel(new ProductoDAO().buscadorTabla(getVistaprincipal().getTxtBuscar().getText()));
			//oculta el id de la tabla
			getVistaprincipal().getTable().getColumnModel().getColumn(0).setMaxWidth(0);
			getVistaprincipal().getTable().getColumnModel().getColumn(0).setMinWidth(0);
			getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
			getVistaprincipal().getTable().getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
		}
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		this.actualizarModeloTabla();
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public VistaPrincipalGeneral getVistaprincipal() {
		return vistaprincipal;
	}

	public void setVistaprincipal(VistaPrincipalGeneral vistaprincipal) {
		this.vistaprincipal = vistaprincipal;
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusLost(FocusEvent e) {
	
		
	}
}
