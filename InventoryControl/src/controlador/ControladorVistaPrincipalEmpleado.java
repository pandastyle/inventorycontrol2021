package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.Producto;
import modelo.ProductoDAO;
import modelo.Usuario;
import modelo.UsuarioEmpleado;
import vista.VistaDetalleProducto;
import vista.VistaNuevoProducto;

import vista.VistaPrincipalGeneral;

public class ControladorVistaPrincipalEmpleado extends ControladorVistaGeneral{
	
	public ControladorVistaPrincipalEmpleado(Usuario usu) {
		super(usu);
		this.setVistaprincipal(new VistaPrincipalGeneral(this));
		this.getVistaprincipal().setVisible(true);
		this.actualizarModeloTabla();
		super.habilitacionBotones(false);
	}

	
}

