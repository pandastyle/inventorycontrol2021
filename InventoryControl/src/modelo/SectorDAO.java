package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import excepciones.ExcepcionSectorExistencia;

public class SectorDAO {
	public ArrayList<Sector> getAll(){
		ArrayList<Sector> sectores = new ArrayList<Sector>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.sector");
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				
				sectores.add(new Sector(id,nombre,estado));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sectores;
	}
	
	public Sector get(Integer id) {
		Sector sector= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.sector WHERE id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				sector=new Sector(idrs,nombre,estado);	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sector;
	}
	
	public Integer insert (Sector s) throws ExcepcionSectorExistencia {
		if(verificarNoExistencia(s)) {
			return BasedeDatos.getInstance().alta("sector", "nombre, estado", "'"+s.getNombre_sector()+"','true'");
		}else {
			throw new ExcepcionSectorExistencia();
		}
		
	}
	
	public Boolean update(Sector s) throws ExcepcionSectorExistencia {
		if(verificarNoExistencia(s)) {
		return BasedeDatos.getInstance().update("sector", s.getId_sector() ,"nombre= '"+s.getNombre_sector()+"', estado= '"+s.getEstado_sector()+"'");
		}else {
			throw new ExcepcionSectorExistencia();
		}
	}
	
	public Boolean baja(Sector s) {
		return BasedeDatos.getInstance().update("sector", s.getId_sector() ,"nombre= '"+s.getNombre_sector()+"', estado= 'false'");
	}
	
	public Boolean verificarNoExistencia(Sector sec) {
		Boolean insertar=true;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.sector WHERE nombre= '"+sec.getNombre_sector()+"'");//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				insertar=false;
				Integer id= rs.getInt("id");
				if (sec.getId_sector()!=null) {
					if (sec.getId_sector()==id) {
						insertar=true;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return insertar;
	}
}
