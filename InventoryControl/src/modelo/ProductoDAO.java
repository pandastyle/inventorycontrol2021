package modelo;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import excepciones.ExcepcionCodigoExistencia;
import excepciones.ExcepcionMarcaExistencia;

public class ProductoDAO {
	
        public ArrayList<Producto> getAll(){
    		ArrayList<Producto> productos = new ArrayList<Producto>();
    		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT pr.id AS id_producto, pr.codigo, pr.nombre, pr.descripcion, \n"
    				+ "mar.nombre AS nombre_marca, cat.nombre AS nombre_categoria, pr.stock, pr.precio,\n"
    				+ "pr.estado_producto, pr.fecha_caduc, pr.foto\n"
    				+ "FROM producto pr JOIN categoria cat on pr.id_categoria = cat.id\n"
    				+ "JOIN marca mar on pr.id_marca = mar.id ORDER BY pr.codigo");  //devuelve los valores haciendo el join con las tablas marca y categoria
    		try {
    			while (rs.next()) {
    				Integer id = rs.getInt("id_producto");
    				Integer cod = rs.getInt("codigo");
    				String nombre = rs.getString("nombre");
    				String descripcion = rs.getString("descripcion");
    				String marca = rs.getString("nombre_marca");  //chequear
    				String categoria = rs.getString("nombre_categoria");  //chequear
    				Date fechaCad = rs.getDate("fecha_caduc"); //cambiado a date en producto para evitar error de datos
    				Integer stock = rs.getInt("stock");
    				Double precio = rs.getDouble("precio");
    				Boolean estado= rs.getBoolean("estado_producto");
    				String foto = rs.getString("foto");
    				
    				
    				productos.add(new Producto(id, cod, nombre, descripcion, marca, categoria, fechaCad, stock, precio , estado, foto));
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
    		return productos;
    	}
        
        public Producto get(Integer id) {
        	Producto producto= null ;
        	
    		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT pr.id AS id_producto, pr.codigo, pr.nombre, pr.descripcion, \n"
    				+ "mar.nombre AS nombre_marca, cat.nombre AS nombre_categoria, pr.stock, pr.precio,\n"
    				+ "pr.estado_producto, pr.fecha_caduc, pr.foto\n"
    				+ "FROM producto pr JOIN categoria cat on pr.id_categoria = cat.id\n"
    				+ "JOIN marca mar on pr.id_marca = mar.id WHERE pr.id="+id); 
    		try {
    			while (rs.next()) {
    				Integer idrs = rs.getInt("id_producto");
    				Integer cod = rs.getInt("codigo");
    				String nombre = rs.getString("nombre");
    				String descripcion = rs.getString("descripcion");
    				String marca = rs.getString("nombre_marca");  
    				String categoria = rs.getString("nombre_categoria");  
    				Date fechaCad =  rs.getDate("fecha_caduc"); 
    				Integer stock = rs.getInt("stock");
    				Double precio = rs.getDouble("precio");
    				Boolean estado= rs.getBoolean("estado_producto");
    				String foto = rs.getString("foto");
    				producto=new Producto(idrs, cod, nombre, descripcion, marca, categoria, fechaCad, stock, precio , estado, foto);	//editado para foto
    			}
    		} catch (SQLException e) {
 
    			e.printStackTrace();
    		}
    		return producto;
    	}
        
        public Integer numImagen() {
        	Integer id=null;
    		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT MAX(id) as id_producto from producto"); 
    		try {
    			while (rs.next()) {
    					id= rs.getInt("id_producto");
    			}
    		} catch (SQLException e) {
    			
    			e.printStackTrace();
    		}
    		if (id==null) {
				id=1;
			}else {
				id++;
			}
    		return id;
    	}
        
        public Integer insert (Producto p) throws ExcepcionCodigoExistencia {
        	if(verificarNoExistencia(p)) {        	
        	if (p.getFecha_caducidad()!=null) {
        		return BasedeDatos.getInstance().alta("producto", "codigo, nombre, descripcion, id_marca, id_categoria, fecha_caduc, stock, precio, estado_producto, foto",
             		   "'"+p.getCod_producto()+"','"+p.getNombre_producto()+"','"+p.getDescripcion()+"','"+obtenerIdMarca(p)+"','"+obtenerIdCategoria(p)+"','"+p.getFecha_caducidad()+"','"+p.getStock()+"','"+
                p.getPrecio()+"','"+"true"+"','"+p.getFoto()+"'");
			}else {
				return BasedeDatos.getInstance().alta("producto", "codigo, nombre, descripcion, id_marca, id_categoria, stock, precio, estado_producto, foto",
	             		   "'"+p.getCod_producto()+"','"+p.getNombre_producto()+"','"+p.getDescripcion()+"','"+obtenerIdMarca(p)+"','"+obtenerIdCategoria(p)+"','"+p.getStock()+"','"+
	                p.getPrecio()+"','"+"true"+"','"+p.getFoto()+"'");
			}
        	}else {
				throw new ExcepcionCodigoExistencia();
			}
		   
		   
        }
    	
        
    
    	public Boolean update(Producto p) throws ExcepcionCodigoExistencia {
    		if(verificarNoExistencia(p)) {   
    		if (p.getFecha_caducidad()!=null) {
    		return BasedeDatos.getInstance().update("producto", p.getId_producto() ,"codigo ='" + p.getCod_producto() + "', nombre= '"+p.getNombre_producto()+ "', descripcion= '"+ p.getDescripcion()+
    				"', id_marca= '"+ obtenerIdMarca(p) +"', id_categoria= '"+ obtenerIdCategoria(p) + "', fecha_caduc= '"+ p.getFecha_caducidad() +"', stock= '"+ p.getStock() + 
    				"', precio= '"+ p.getPrecio() + "', estado_producto= '"+p.getEstado_producto() + "', foto= '" + p.getFoto()+"'");
    		}else {
    			return BasedeDatos.getInstance().update("producto", p.getId_producto() ,"codigo ='" + p.getCod_producto() + "', nombre= '"+p.getNombre_producto()+ "', descripcion= '"+ p.getDescripcion()+
        				"', id_marca= '"+ obtenerIdMarca(p) +"', id_categoria= '"+ obtenerIdCategoria(p) +"', stock= '"+ p.getStock() + 
        				"', precio= '"+ p.getPrecio() + "', estado_producto= '"+p.getEstado_producto() + "', foto= '" + p.getFoto()+"'");
    		}
    		}else {
				throw new ExcepcionCodigoExistencia();
			}
    	}
    	
    		public Boolean baja(Producto p) {
    			
    		return BasedeDatos.getInstance().update("producto",p.getId_producto(), "estado_producto='false'");
    	}
    	
    	public Integer obtenerIdCategoria(Producto p) {
    		CategoriaDAO cat = new CategoriaDAO();
    		ArrayList<Categoria> categorias = cat.getAll(); 
    		Integer idCat = null;
    		for (Categoria categoria : categorias) {
				if (categoria.getNombre_categoria().equals(p.getNombre_categoria())) {
					idCat = categoria.getId_categoria();
				}
			}
    		return idCat;
    	}
        public Integer obtenerIdMarca(Producto p) {
        	MarcaDAO marca = new MarcaDAO();
    		ArrayList<Marca> marcas = marca.getAll(); 
    		Integer idMarca = null;
    		for (Marca marca2 : marcas) {
				if (marca2.getNombre_marca().equals(p.getNombre_marca())) {
					idMarca = marca2.getId_marca();
				}
			}
    		return idMarca;
    	}
 
        //NOTA CONSISTIR QUE LA FECHA INGRESADA PARA LA CADUCIDAD SEA ANTES DE LA FECHA ACTUAL

     /*   public Integer getIdCod(Integer codigo) {//creado solo por que es necesario tenerlo para el nombre de la foto en modificar
        	Integer id= null ;
    		ResultSet rs= BasedeDatos.getInstance().getAll("Select id from producto where codigo="+codigo); 
    		try {
    			while (rs.next()) {
    				id = rs.getInt("id");
    			}
    		} catch (SQLException e) {
 
    			e.printStackTrace();
    		}
    		return id;
    	}*/
        
        public DefaultTableModel buscadorTabla(String buscar) {
        	
        	String cabecera[]= {"id","C�digo","Producto","Categor�a","Marca","Stock"};
    		DefaultTableModel modelo= new DefaultTableModel(null,cabecera) {
    			@Override
    			public boolean isCellEditable(int row, int column) {
    			if (column==6) {
    				return true;
    			} else {
    				return false;
    			}
    		}
    		};
        	
    		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT pr.id AS id_producto, pr.codigo, pr.nombre,"
    				+ "mar.nombre AS nombre_marca, cat.nombre AS nombre_categoria, pr.stock,pr.estado_producto "
    				+ "FROM producto pr JOIN categoria cat on pr.id_categoria = cat.id "
    				+ "JOIN marca mar on pr.id_marca = mar.id "
    				+ "WHERE CAST(pr.codigo as varchar(10)) LIKE '%"+buscar+"%' OR "
    					  + "pr.nombre LIKE '%"+buscar+"%' OR "
    					  + "mar.nombre LIKE '%"+buscar+"%' OR "
    					  + "cat.nombre LIKE '%"+buscar+"%' "); 
    		try {
    			while (rs.next()) {
    				Boolean estado=rs.getBoolean("estado_producto");
    				if(estado) {
    					Vector<Object> vector = new Vector<Object>();
    					Integer idrs = rs.getInt("id_producto");
        				Integer cod = rs.getInt("codigo");
        				String nombre = rs.getString("nombre");
        				String categoria = rs.getString("nombre_categoria");  
        				String marca = rs.getString("nombre_marca");  
        				Integer stock = rs.getInt("stock");
        				
        				vector.add(idrs);
        				vector.add(cod);
        				vector.add(nombre);
        				vector.add(categoria);
        				vector.add(marca);
        				vector.add(stock);
        				modelo.addRow(vector);
    				}		
    			}
    		} catch (SQLException e) {
 
    			e.printStackTrace();
    		}
    		return modelo;
    	}
        
        public Boolean verificarNoExistencia(Producto p) {
    		Boolean insertar=true;
    		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.producto WHERE codigo="+p.getCod_producto());//devuelve un resultset
    		try {
    			while (rs.next()) {//recorre la tabla
    				insertar=false;
    				Integer cod= rs.getInt("codigo");
    				Integer id= rs.getInt("id");
    				
    				if(p.getId_producto()!=null) {
    					if (p.getCod_producto().equals(cod)&& p.getId_producto()==id) {
    						insertar=true;
    					}
    				}
    					
    				
    			}
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		return insertar;
    	}
}
	





