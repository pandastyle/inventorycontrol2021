package modelo;

public class Categoria {
	private Integer id_categoria;
	private String nombre_categoria;
	private Boolean estado_categoria;
	
	public Categoria(Integer id_categoria, String nombre_categoria, Boolean estado_categoria) {
		
		this.id_categoria = id_categoria;
		this.nombre_categoria = nombre_categoria;
		this.estado_categoria = estado_categoria;
	}

	public Categoria() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(Integer id_categoria) {
		this.id_categoria = id_categoria;
	}

	public String getNombre_categoria() {
		return nombre_categoria;
	}

	public void setNombre_categoria(String nombre_categoria) {
		this.nombre_categoria = nombre_categoria;
	}

	public Boolean getEstado_categoria() {
		return estado_categoria;
	}

	public void setEstado_categoria(Boolean estado_categoria) {
		this.estado_categoria = estado_categoria;
	}
	
	
	
	

}
