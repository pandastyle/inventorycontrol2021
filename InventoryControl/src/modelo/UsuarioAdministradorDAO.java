package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import excepciones.ExcepcionUsuarioExistencia;



public class UsuarioAdministradorDAO {
	public ArrayList<UsuarioAdministrador> getAll(){
		ArrayList<UsuarioAdministrador> usuadministradores = new ArrayList<UsuarioAdministrador>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.usuarios");
		try {
			while (rs.next()) {
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido_usuario");
				String password= rs.getString("contrasenia");
				String contacto= rs.getString("contacto");
				String correo= rs.getString("correo");
				Boolean estado= rs.getBoolean("estado_cuenta");
				String tipo_usuario= rs.getString("tipo_usuario");
				Integer usuario = rs.getInt("usuario");
				
				usuadministradores.add(new UsuarioAdministrador(idrs, usuario, password, nombre,apellido, contacto, correo, tipo_usuario, estado));
				
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuadministradores;
	}
	
	public UsuarioAdministrador get(Integer id) {
		UsuarioAdministrador administrador= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuarios WHERE id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido_usuario");
				String password= rs.getString("contrasenia");
				String contacto= rs.getString("contacto");
				String correo= rs.getString("correo");
				Boolean estado= rs.getBoolean("estado_cuenta");
				String tipo_usuario= rs.getString("tipo_usuario");
				Integer usuario = rs.getInt("usuario");
				administrador=new UsuarioAdministrador(idrs, usuario, password, nombre,apellido, contacto, correo, tipo_usuario, estado);
					
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return administrador;
	}
	
	public Integer insert (UsuarioAdministrador u) throws ExcepcionUsuarioExistencia {
		if(verificarNoExistencia(u)) {
		return BasedeDatos.getInstance().alta("usuarios", "nombre, contrasenia, contacto, correo, estado_cuenta, tipo_usuario, usuario, apellido_usuario"
				, "'"+u.getNombre_usuario()+"','"+u.getPassword()+"','"+u.getContacto_usuario()+"','"+u.getCorreo_usuario()+"','true'"+",'"+u.getTipo_usuario()+"','"+u.getUsuario()+"','"+u.getApellido_usuario()+"'");
		}else {
			throw new ExcepcionUsuarioExistencia();
		}
		}
	
	public Boolean update(UsuarioAdministrador u) throws ExcepcionUsuarioExistencia {
		if(verificarNoExistencia(u)) {
		return BasedeDatos.getInstance().update("usuarios", u.getId_usuario() ,"nombre= '"+u.getNombre_usuario()+"', contrasenia= '"+u.getPassword()+"', contacto= '"+u.getContacto_usuario()+
							"', correo= '"+u.getCorreo_usuario()+"', tipo_usuario= '"+u.getTipo_usuario()+"', usuario= '"+u.getUsuario()+"', apellido_usuario= '"+u.getApellido_usuario()+"', estado_cuenta= 'true'");
		}else {
			throw new ExcepcionUsuarioExistencia();
		}
		}
	
	public Boolean baja(UsuarioAdministrador u) {
		return BasedeDatos.getInstance().update("usuarios", u.getId_usuario() ,"estado_cuenta= 'false'");
	}
	

	public Usuario logeo(Integer usu,String pass) {
		Usuario usuarioLogin= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuarios WHERE usuario="+usu+" and contrasenia='"+pass+"' and estado_cuenta= 'true'");//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido_usuario");
				String password= rs.getString("contrasenia");
				String contacto= rs.getString("contacto");
				String correo= rs.getString("correo");
				Boolean estado= rs.getBoolean("estado_cuenta");
				String tipo_usuario= rs.getString("tipo_usuario");
				Integer usuario = rs.getInt("usuario");
				usuarioLogin =new Usuario(idrs, usuario, password, nombre,apellido, contacto, correo, tipo_usuario, estado);
					
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarioLogin;
	}
	
	public DefaultTableModel buscadorTablaUsuario(String buscar) {
	String cabecera[]= {"id","Usuario","Nombre Empleado","Correo","Rol","Sector"};
	DefaultTableModel modelo= new DefaultTableModel(null,cabecera) {
		@Override
		public boolean isCellEditable(int row, int column) {
		if (column==6) {
			return true;
		} else {
			return false;
		}
	}
	};
	
	ResultSet rs= BasedeDatos.getInstance().getAll("SELECT usu.id, usu.usuario, usu.nombre, usu.apellido_usuario , usu.correo, usu.tipo_usuario, usu.sector_emp, estado_cuenta "
			+ "from usuarios usu "
			+ "where CAST(usuario as varchar(10)) LIKE '%"+buscar+"%' "
			+ "OR nombre LIKE '%"+buscar+"%'");
	
	try {
		while (rs.next()) {
			Boolean flag= rs.getBoolean("estado_cuenta");
			if(flag) {
				Vector<Object> vector = new Vector<Object>();
				Integer idrs= rs.getInt("id");
				Integer usuario= rs.getInt("usuario");
				String nombre_completo= rs.getString("nombre")+" "+rs.getString("apellido_usuario");
				String correo= rs.getString("correo");
				String tipo_usuario= rs.getString("tipo_usuario");
				String sector="";
				Integer sec= rs.getInt("sector_emp");
				if(sec!=null && sec!=0) {
					
					sector= new SectorDAO().get(sec).getNombre_sector();
				}else {
					sector="----------";
				}
				
				vector.add(idrs);
				vector.add(usuario);
				vector.add(nombre_completo);
				vector.add(correo);
				vector.add(tipo_usuario);
				vector.add(sector);
			
				
				modelo.addRow(vector);
			}
		}
	} catch (SQLException e) {

		e.printStackTrace();
		
	}
	return modelo;
	}
	
	public Boolean verificarNoExistencia(UsuarioAdministrador u) {
 		Boolean insertar=true;
 		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuarios WHERE usuario="+u.getUsuario());//devuelve un resultset
 		try {
 			while (rs.next()) {//recorre la tabla
 				insertar=false;
 				Integer usuario= rs.getInt("usuario");
 				Integer id= rs.getInt("id");
 				if (u.getId_usuario()!=null) {
 					if (u.getUsuario().equals(usuario)&&u.getId_usuario().equals(id)) {
 						insertar=true;
 					}
				}
 					
 			}
 		} catch (SQLException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 		return insertar;
 	}
}
