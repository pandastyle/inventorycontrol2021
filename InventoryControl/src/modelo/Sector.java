package modelo;

public class Sector {
	private Integer id_sector;
	private String nombre_sector;
	private Boolean estado_sector;
	
	
	public Sector(Integer id_sector, String nombre_sector, Boolean estado_sector) {
		this.id_sector = id_sector;
		this.nombre_sector = nombre_sector;
		this.estado_sector = estado_sector;
	}
	
	public Sector() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId_sector() {
		return id_sector;
	}
	public void setId_sector(Integer id_sector) {
		this.id_sector = id_sector;
	}
	public String getNombre_sector() {
		return nombre_sector;
	}
	public void setNombre_sector(String nombre_sector) {
		this.nombre_sector = nombre_sector;
	}
	public Boolean getEstado_sector() {
		return estado_sector;
	}
	public void setEstado_sector(Boolean estado_sector) {
		this.estado_sector = estado_sector;
	}
	
	
}
