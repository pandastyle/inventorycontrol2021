package modelo;

public class Marca {
	private Integer id_marca;
	private String nombre_marca;
	private Boolean estado_marca;
	
	public Marca(Integer id_marca, String nombre_marca, Boolean estado_marca) {
		
		this.id_marca = id_marca;
		this.nombre_marca = nombre_marca;
		this.estado_marca = estado_marca;
	}

	public Marca() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId_marca() {
		return id_marca;
	}

	public void setId_marca(Integer id_marca) {
		this.id_marca = id_marca;
	}

	public String getNombre_marca() {
		return nombre_marca;
	}

	public void setNombre_marca(String nombre_marca) {
		this.nombre_marca = nombre_marca;
	}

	public Boolean getEstado_marca() {
		return estado_marca;
	}

	public void setEstado_marca(Boolean estado_marca) {
		this.estado_marca = estado_marca;
	}
	
	
	
	
}
