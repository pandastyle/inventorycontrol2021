package modelo;

import java.sql.Date;
//import java.time.LocalDate;

public class Producto {
	private Integer id_producto;
	private Integer cod_producto;
	private String nombre_producto;
	private String descripcion;
	private String nombre_marca;//en la bd tenemos el id de la marca para hacer referencia a la tabla
	private String nombre_categoria;//lo mismo que marca
	private Date fecha_caducidad;
	private Integer stock;
	private Double precio;
	private Boolean estado_producto;
	private String foto; 
	
	public Producto(Integer id_producto, Integer cod_producto, String nombre_producto, String descripcion,
			String nombre_marca, String nombre_categoria, Date fecha_caducidad, Integer stock, Double precio,
			Boolean estado_producto, String foto) {
		super();
		this.id_producto = id_producto;
		this.cod_producto = cod_producto;
		this.nombre_producto = nombre_producto;
		this.descripcion = descripcion;
		this.nombre_marca = nombre_marca;
		this.nombre_categoria = nombre_categoria;
		this.fecha_caducidad = fecha_caducidad;
		this.stock = stock;
		this.precio = precio;
		this.estado_producto = estado_producto;
		this.foto = foto;
	}
	
	//para productos sin caducidad
	public Producto(Integer id_producto, Integer cod_producto, String nombre_producto, String descripcion,
			String nombre_marca, String nombre_categoria,  Integer stock, Double precio,
			Boolean estado_producto, String foto) {
		super();
		this.id_producto = id_producto;
		this.cod_producto = cod_producto;
		this.nombre_producto = nombre_producto;
		this.descripcion = descripcion;
		this.nombre_marca = nombre_marca;
		this.nombre_categoria = nombre_categoria;
		this.stock = stock;
		this.precio = precio;
		this.estado_producto = estado_producto;
		this.foto = foto;
	}
	


	public Integer getId_producto() {
		return id_producto;
	}
	public void setId_producto(Integer id_producto) {
		this.id_producto = id_producto;
	}
	public Integer getCod_producto() {
		return cod_producto;
	}
	public void setCod_producto(Integer cod_producto) {
		this.cod_producto = cod_producto;
	}
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombre_marca() {
		return nombre_marca;
	}
	public void setNombre_marca(String nombre_marca) {
		this.nombre_marca = nombre_marca;
	}
	public String getNombre_categoria() {
		return nombre_categoria;
	}
	public void setNombre_categoria(String nombre_categoria) {
		this.nombre_categoria = nombre_categoria;
	}
	public Date getFecha_caducidad() {
		return fecha_caducidad;
	}
	public void setFecha_caducidad(Date fecha_caducidad) {
		this.fecha_caducidad = fecha_caducidad;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Boolean getEstado_producto() {
		return estado_producto;
	}
	public void setEstado_producto(Boolean estado_producto) {
		this.estado_producto = estado_producto;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	
	
	
}
