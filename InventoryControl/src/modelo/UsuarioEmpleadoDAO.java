package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import excepciones.ExcepcionUsuarioExistencia;

public class UsuarioEmpleadoDAO {
	public ArrayList<UsuarioEmpleado> getAll(){
		ArrayList<UsuarioEmpleado> usuEmpleados = new ArrayList<UsuarioEmpleado>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT usu.id, usu.nombre, contrasenia, contacto, correo, estado_cuenta,tipo_usuario, sector.nombre as nombre_sector,usuario,apellido_usuario "
				+ "FROM public.usuarios usu JOIN public.sector ON usu.sector_emp = sector.id");
		
		try {
			while (rs.next()) {
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido_usuario");
				String password= rs.getString("contrasenia");
				String contacto= rs.getString("contacto");
				String correo= rs.getString("correo");
				Boolean estado= rs.getBoolean("estado_cuenta");
				String tipo_usuario= rs.getString("tipo_usuario");
				Integer usuario = rs.getInt("usuario");
				String sector_emp= rs.getString("nombre_sector");
				
				usuEmpleados.add(new UsuarioEmpleado(idrs, usuario, password, nombre,apellido, contacto, correo, tipo_usuario, estado,sector_emp));
				
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuEmpleados;
	}
	
	public UsuarioEmpleado get(Integer id) {
		UsuarioEmpleado empleado= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT usu.id, usu.nombre, contrasenia, contacto, correo, estado_cuenta,tipo_usuario, sector.nombre as nombre_sector,usuario,apellido_usuario"
		+" FROM public.usuarios usu JOIN public.sector ON usu.sector_emp = sector.id WHERE usu.id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido_usuario");
				String password= rs.getString("contrasenia");
				String contacto= rs.getString("contacto");
				String correo= rs.getString("correo");
				Boolean estado= rs.getBoolean("estado_cuenta");
				String tipo_usuario= rs.getString("tipo_usuario");
				Integer usuario = rs.getInt("usuario");
				String sector_emp= rs.getString("nombre_sector");
				empleado=new UsuarioEmpleado(idrs, usuario, password, nombre,apellido, contacto, correo, tipo_usuario, estado, sector_emp);
					
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return empleado;
	}
	
	public Integer insert (UsuarioEmpleado u) throws ExcepcionUsuarioExistencia {
				if(verificarNoExistencia(u)) {
					return BasedeDatos.getInstance().alta("usuarios", "nombre, contrasenia, contacto, correo, estado_cuenta, tipo_usuario, usuario, apellido_usuario,sector_emp"
							, "'"+u.getNombre_usuario()+"','"+u.getPassword()+"','"+u.getContacto_usuario()+"','"+u.getCorreo_usuario()+"','true'"+",'"+u.getTipo_usuario()+"','"+u.getUsuario()+"','"+u.getApellido_usuario()+"','"+obtenerIdSector(u)+"'");
				}else {
					throw new ExcepcionUsuarioExistencia();
				}
		
	}
	
	public Boolean update(UsuarioEmpleado u) throws ExcepcionUsuarioExistencia {
		if(verificarNoExistencia(u)) {
		return BasedeDatos.getInstance().update("usuarios", u.getId_usuario() ,"nombre= '"+u.getNombre_usuario()+"', contrasenia= '"+u.getPassword()+"', contacto= '"+u.getContacto_usuario()+
							"', correo= '"+u.getCorreo_usuario()+"', tipo_usuario= '"+u.getTipo_usuario()+"', usuario= '"+u.getUsuario()+"', apellido_usuario= '"+u.getApellido_usuario()+"', estado_cuenta= 'true'"+", sector_emp= '"+obtenerIdSector(u)+"'");
		}else {
			throw new ExcepcionUsuarioExistencia();
		}

		}
	
	public Boolean baja(UsuarioEmpleado u) {
		return BasedeDatos.getInstance().update("usuarios", u.getId_usuario() ,"estado_cuenta= 'false'");
	}
	
	public Integer obtenerIdSector(UsuarioEmpleado u) {
		ArrayList<Sector> sectores= new SectorDAO().getAll();
		Integer idsector=0;
		for (Sector sector : sectores) {
			if (u.getSector_empleado().equals(sector.getNombre_sector())) {
				idsector=sector.getId_sector();
			}
		}
		return idsector;
	}
	
	 public Boolean verificarNoExistencia(UsuarioEmpleado u) {
 		Boolean insertar=true;
 		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuarios WHERE usuario="+u.getUsuario());//devuelve un resultset
 		try {
 			while (rs.next()) {//recorre la tabla
 				insertar=false;
 				Integer usuario= rs.getInt("usuario");
 				Integer id= rs.getInt("id");
 				if (u.getId_usuario()!=null) {
 					if (u.getUsuario().equals(usuario)&&u.getId_usuario().equals(id)) {
 						insertar=true;
 					}
				}
 			}
 		} catch (SQLException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 		return insertar;
 	}
	
	
}
