package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import excepciones.ExcepcionMarcaExistencia;

public class MarcaDAO {

	public ArrayList<Marca> getAll(){
		ArrayList<Marca> marcas = new ArrayList<Marca>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.marca");
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				
				marcas.add(new Marca(id,nombre,estado));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return marcas;
	}
	
	public Marca get(Integer id) {
		Marca marca= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.marca WHERE id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				marca=new Marca(idrs,nombre,estado);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return marca;
	}
	
	public Integer insert (Marca m) throws ExcepcionMarcaExistencia {
		if(verificarNoExistencia(m)) {
			return BasedeDatos.getInstance().alta("marca", "nombre, estado", "'"+m.getNombre_marca()+"','true'");
		}else {
			throw new ExcepcionMarcaExistencia();
		}
		
	}
	
	public Boolean update(Marca m) throws ExcepcionMarcaExistencia {
		if(verificarNoExistencia(m)) {
		return BasedeDatos.getInstance().update("marca", m.getId_marca() ,"nombre= '"+m.getNombre_marca()+"', estado= '"+m.getEstado_marca()+"'");
		}else {
			throw new ExcepcionMarcaExistencia();
		}
		}
	
	public Boolean baja(Marca m) {
		return BasedeDatos.getInstance().update("marca", m.getId_marca() ,"nombre= '"+m.getNombre_marca()+"', estado= 'false'");
	}
	
	public Boolean verificarNoExistencia(Marca m) {
		Boolean insertar=true;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.marca WHERE nombre= '"+m.getNombre_marca()+"'");//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				insertar=false;
				Integer id= rs.getInt("id");
				if (m.getId_marca()!=null) {
					if (m.getId_marca()==id) {
						insertar=true;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return insertar;
	}
}
