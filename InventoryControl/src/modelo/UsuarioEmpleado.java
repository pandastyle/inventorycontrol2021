package modelo;

import java.util.ArrayList;

public class UsuarioEmpleado extends Usuario {
	
	private String sector_empleado;

	public UsuarioEmpleado(Integer id_usuario, Integer usuario, String password, String nombre_usuario,String apellido_usuario,
			String contacto_usuario, String correo_usuario, String tipo_usuario,
			Boolean estado_cuenta,String sector_empleado) {
		super(id_usuario, usuario, password, nombre_usuario,apellido_usuario, contacto_usuario, correo_usuario, tipo_usuario,
				estado_cuenta);
		this.setSector_empleado(sector_empleado);
	}

	public String getSector_empleado() {
		return sector_empleado;
	}

	public void setSector_empleado(String sector_empleado) {
		this.sector_empleado = sector_empleado;
	}

	@Override
	public String toString() {
		return super.toString()+"\nUsuarioEmpleado [sector_empleado=" + sector_empleado + "]";
	}
	

}
