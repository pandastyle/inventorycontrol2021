package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import excepciones.ExcepcionCategoriaExistencia;

public class CategoriaDAO {

	public ArrayList<Categoria> getAll(){
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.categoria");
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				
				categorias.add(new Categoria(id,nombre,estado));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categorias;
	}
	
	public Categoria get(Integer id) {
		Categoria categoria= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.categoria WHERE id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				Boolean estado= rs.getBoolean("estado");
				categoria=new Categoria(idrs,nombre,estado);	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categoria;
	}
	
	public Integer insert (Categoria c) throws ExcepcionCategoriaExistencia {
		if(verificarNoExistencia(c)) {
		return BasedeDatos.getInstance().alta("categoria", "nombre, estado", "'"+c.getNombre_categoria()+"','true'");
		}else {
			throw new ExcepcionCategoriaExistencia();
		}
	}
	
	public Boolean update(Categoria c) throws ExcepcionCategoriaExistencia {
		if(verificarNoExistencia(c)) {
		return BasedeDatos.getInstance().update("categoria", c.getId_categoria() ,"nombre= '"+c.getNombre_categoria()+"', estado= '"+c.getEstado_categoria()+"'");
		}else {
			throw new ExcepcionCategoriaExistencia();
		}
		}
	
	public Boolean baja(Categoria c) {
		return BasedeDatos.getInstance().update("categoria", c.getId_categoria() ,"nombre= '"+c.getNombre_categoria()+"', estado= 'false'");
	}
	
	public Boolean verificarNoExistencia(Categoria c) {
		Boolean insertar=true;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.categoria WHERE nombre= '"+c.getNombre_categoria()+"'");
		try {
			while (rs.next()) {//recorre la tabla
				Integer id= rs.getInt("id");
				insertar=false;
				if (c.getId_categoria()!=null) {
					if (c.getId_categoria()==id) {
						insertar=true;
					}
				}
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return insertar;
	}
}
