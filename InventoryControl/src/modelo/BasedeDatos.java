package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BasedeDatos {
   private Connection conexion;
   private String url= "jdbc:postgresql://localhost/inventorycontrol";
   private String usuario="postgres";
   private String clave="postgres";
   private static BasedeDatos bd = null;
   
   private BasedeDatos() {
	   
	   try {//conecta la base de datos
			DriverManager.registerDriver(new org.postgresql.Driver());
			this.setConexion(DriverManager.getConnection(url,usuario,clave));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	   
   }


public Connection getConexion() {
	return conexion;
}

	private void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	
   public static BasedeDatos getInstance() {//SINGLETON
	   //if corto de singleton
	   return (bd==null)? new BasedeDatos():bd; 
   }
   
   public ResultSet getAll(String consulta) {//obtiene datos de la base
	   ResultSet rs = null;
	   
	   try {
	    Statement s = conexion.createStatement();
		rs = s.executeQuery(consulta);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	   return rs;
   }
   
   public Integer alta(String tabla,String columnas,String valores) {// valido para insert 
	  
	   try {
		PreparedStatement ps = conexion.prepareStatement("INSERT INTO public."+tabla+" ("+columnas+") VALUES ("+valores+") returning id;");//devuelve el id
		if (ps.execute()) {
			ResultSet rs= ps.getResultSet();
			rs.next();//avanza el puntero
			return rs.getInt(1);	
		}else {
		  Integer entero= ps.getUpdateCount();		 
		}
	} catch (SQLException e) {
		e.printStackTrace();
	} return -1;
		   
   }
   
   public Boolean update(String tabla, Integer id, String valores) { //MODIFICACION
	   	
	   	try {
	   		PreparedStatement ps = conexion.prepareStatement("UPDATE public." + tabla + " SET "+valores+" WHERE id="+id+";");
	   		
	   		ps.execute();
	   		Integer entero = ps.getUpdateCount();
	   		return entero>0;
	   	} catch (SQLException e) {
	   		e.printStackTrace();
	   	} return false;
	   }
   
   public Boolean remove(String tabla, Integer id) {
	   //DELETE FROM public.usuario WHERE id IN (3);
	   
	   try {
		Statement st= conexion.createStatement();
		 Integer entero= st.executeUpdate("DELETE FROM public."+tabla+" WHERE id IN ("+id+");");
		 return entero>0;
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	   return false;
   }
}
