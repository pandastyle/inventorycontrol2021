package modelo;

import java.util.ArrayList;

public class Usuario {

	 private Integer id_usuario;
	 private Integer usuario;//usuario=dni
	 private String password;
	 private String nombre_usuario;//nombre de la persona
	 private String apellido_usuario;
	 private String contacto_usuario;
	 private String correo_usuario;
	 private String tipo_usuario;//puede ser empleado o algun tipo de administrador
	 private Boolean estado_cuenta;
	 
	 
	public Usuario(Integer id_usuario, Integer usuario, String password, String nombre_usuario,String apellido_usuario, String contacto_usuario,
			String correo_usuario, String tipo_usuario, Boolean estado_cuenta) {
		super();
		this.id_usuario = id_usuario;
		this.usuario = usuario;
		this.setPassword(password);
		this.nombre_usuario = nombre_usuario;
		this.apellido_usuario= apellido_usuario;
		this.contacto_usuario = contacto_usuario;
		this.correo_usuario = correo_usuario;
		this.tipo_usuario = tipo_usuario;
		//this.productos = productos;
		this.estado_cuenta = estado_cuenta;
	}
	
	
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Integer getUsuario() {
		return usuario;
	}
	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		
		this.password = password;
	}
	public String getNombre_usuario() {
		return nombre_usuario;
	}
	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}
	public String getContacto_usuario() {
		return contacto_usuario;
	}
	public void setContacto_usuario(String contacto_usuario) {
		this.contacto_usuario = contacto_usuario;
	}
	public String getCorreo_usuario() {
		return correo_usuario;
	}
	public void setCorreo_usuario(String correo_usuario) {
		this.correo_usuario = correo_usuario;
	}
	public String getTipo_usuario() {
		return tipo_usuario;
	}
	public void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}
	public Boolean getEstado_cuenta() {
		return estado_cuenta;
	}
	public void setEstado_cuenta(Boolean estado_cuenta) {
		this.estado_cuenta = estado_cuenta;
	}


	public String getApellido_usuario() {
		return apellido_usuario;
	}


	public void setApellido_usuario(String apellido_usuario) {
		this.apellido_usuario = apellido_usuario;
	}


	@Override
	public String toString() {
		return "Usuario [id_usuario=" + id_usuario + ", usuario=" + usuario + ", password=" + password
				+ ", nombre_usuario=" + nombre_usuario + ", apellido_usuario=" + apellido_usuario
				+ ", contacto_usuario=" + contacto_usuario + ", correo_usuario=" + correo_usuario + ", tipo_usuario="
				+ tipo_usuario + ", estado_cuenta=" + estado_cuenta + "]";
	}
	
	
	
	 
}
