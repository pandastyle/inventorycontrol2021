package modelo;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
public class Utils {
	public static void copyFile(String from, String to) throws IOException{
		 Path origen = Paths.get(from);
		 Path destino= Paths.get(to);
		 Files.copy(origen, destino, StandardCopyOption.REPLACE_EXISTING);
		}
}
