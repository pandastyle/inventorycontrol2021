package excepciones;

public class Mensajes {
	public static String confirmacionPassword= "Las contrase�as no coinciden";
	public static String verificacionCamposVacios= "Verifique que los campos obligatorios * no est�n vac�os";
	public static String Login= "El DNI y/o contrase�a son incorrectos";
	public static String fecha_Caducidad= "La fecha de caducidad debe ser posterior a la fecha actual";
	public static String fecha= "Ingrese un formato v�lido de fecha";
	public static String tipo_datos="Tipo de datos inv�lido, verifique que los datos sean correctos";
	public static String sectorExistencia="El Sector ya existe";
	public static String marcaExistencia="La marca ya existe";
	public static String categoriaExistencia="La categor�a ya existe";
	public static String codigoExistencia="Ya existe un producto con el mismo c�digo";
	public static String usuarioExistencia="El Usuario ya existe";
	public static String negativoCero="Los campos c�digo, stock y precio no pueden ser menor o igual a 0 ";
	public static String sectorValido= "Verifique que el sector sea v�lido";
	
}
